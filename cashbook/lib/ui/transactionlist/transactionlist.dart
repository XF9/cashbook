import 'package:flutter/material.dart';

import '../../generated/i18n.dart';
import '../../helper/navigationHelper.dart';
import '../../helper/transactionHelper.dart';
import '../../provider/transactionProvider.dart';
import '../base.dart';
import '../shared/endDrawerFilter.dart';
import '../shared/filterHeader.dart';
import '../shared/groups/selectGroup.dart';
import '../shared/noData.dart';
import '../shared/openEndDrawerButton.dart';
import '../transactionlist/transactiongroup.dart' as ui;

class TransactionList extends StatefulWidget {
  const TransactionList({Key key}) : super(key: key);

  @override
  _TransactionListState createState() => _TransactionListState();
}

class _TransactionListState extends State<TransactionList> {

  GroupTransactionsByDate _groupBy = GroupTransactionsByDate.month;
  List<TransactionGroup> _data = [];

  @override void initState(){
    super.initState();
    TransactionProvider.instance.startObserving(_refreshData);
    _refreshData();
  }

  @override void dispose(){
    super.dispose();
    TransactionProvider.instance.endObserving(_refreshData);
  }

  Future _refreshData() async {
    final data = await TransactionProvider.instance.getTransactions();
    final groupedData = TransactionHelper().groupTransactionsByDate(data, _groupBy);
    if(mounted)
      setState(() {
        _data = groupedData;
      });
  }

  void _onGroupChange(GroupTransactionsByDate groupBy){
    setState(() {
      _groupBy = groupBy;
      _refreshData(); 
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      title: I18n.of(context).pagesTransactionList,
      currentRoute: Routes.TransactionList,
      appBarActions: <Widget>[
        OpenEndDrawerButton()
      ],
      endDrawer: EndDrawerFilter(
        children: <Widget>[
          FilterHeader(),
          SelectGroup(
            I18n.of(context).transactionsFilterGroupByTitle,
            _groupBy,
            [
              SelectGroupItem(
                label: I18n.of(context).transactionsFilterGroupByDay,
                value: GroupTransactionsByDate.day
              ),
              SelectGroupItem(
                label: I18n.of(context).transactionsFilterGroupByMonth,
                value: GroupTransactionsByDate.month
              ),
              SelectGroupItem(
                label: I18n.of(context).transactionsFilterGroupByYear,
                value: GroupTransactionsByDate.year
              ),
            ],
            _onGroupChange)
        ],
      ),
      child: ListView(
        children: _data.isEmpty
          ? [ const NoData() ]
          : _data.map((item) => ui.TransactionGroup(item)).toList(),
      )
    );    
  }
}