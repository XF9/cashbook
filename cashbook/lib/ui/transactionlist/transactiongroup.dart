import 'package:flutter/material.dart';

import '../../helper/extensions.dart';
import '../../helper/transactionHelper.dart' as helper;
import '../constants.dart';
import 'transactiongroupHeader.dart';
import 'transactiongroupTransactionDetail.dart';

class TransactionGroup extends StatelessWidget {
  const TransactionGroup(this.data, {Key key}) : super(key: key);

  final helper.TransactionGroup data;

  @override
  Widget build(BuildContext context) {

    final List<Widget> dataGroups = [];
    data.transactions.groupBy((x) => x.date).forEach((date, transactions) {
      dataGroups.add(
      Padding(
        padding: const EdgeInsets.symmetric(vertical: spacingUnit),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
              child: Text(date.asShortDate(context),
                style: Theme.of(context)
                  .textTheme
                  .bodyText2
              )
            ),
            ...transactions.map((t) => Padding(
              padding: contentPaddingDense,
              child: TransactiongroupTransactionDetail(t),
            ))
          ],
        ),
      ));
    });

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: spacingUnit * .1),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          title: TransactiongroupHeader(data),
          children: dataGroups,
        ),
      ),
    );
  }
}
