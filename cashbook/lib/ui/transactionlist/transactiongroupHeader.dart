import 'package:flutter/material.dart';

import '../../helper/extensions.dart';
import '../../helper/transactionHelper.dart' as helper;
import '../constants.dart';

class TransactiongroupHeader extends StatelessWidget {

  final helper.TransactionGroup transactionGroup;

  const TransactiongroupHeader(this.transactionGroup, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Row(
      children: <Widget>[
        Expanded(
          child: Text(
            transactionGroup.labelFunc(context),
            style: Theme.of(context).textTheme.headline6,
          )
        ),
        Text(
          transactionGroup.total.asCurrency(context),
          style: Theme.of(context).textTheme.headline6.apply(color: transactionGroup.total < 0 ? expenseColor : incomeColor),
        )
      ],
    );
  }
}