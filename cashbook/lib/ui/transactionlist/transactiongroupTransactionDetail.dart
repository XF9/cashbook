import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../database/database.dart';
import '../../generated/i18n.dart';
import '../../helper/extensions.dart';
import '../../helper/navigationHelper.dart';
import '../../provider/transactionProvider.dart';
import '../constants.dart';
import '../shared/categoryIndicator.dart';

enum TransactionOption { delete, edit }

class TransactiongroupTransactionDetail extends StatelessWidget {
  final Transaction transaction;

  const TransactiongroupTransactionDetail(this.transaction, {Key key})
      : super(key: key);

  void _delete(BuildContext context, Transaction trans) {
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(I18n.of(context).transactionsDeleteConfirmTitle),
            content: Text(
              I18n.of(context).transactionsDeleteConfirmDescription(
                trans.date.asShortDate(context),
                trans.store.name, 
                trans.value.asCurrency(context)
              )
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  I18n.of(context).transactionsDeleteConfirmButtonCancel,
                  style: Theme.of(context).textTheme.button.copyWith(fontWeight: FontWeight.normal),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text(
                  I18n.of(context).transactionsDeleteConfirmButtonOkay,
                  style: Theme.of(context).textTheme.button.copyWith(color: dangerColor),
                ),
                onPressed: () {
                  TransactionProvider.instance.deleteTransaction(trans.id);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: const SlidableBehindActionPane(),
      secondaryActions: <Widget>[
        SlideAction(
          child: const Icon(Icons.edit),
          onTap: () => NavigationHelper.navigateTo(context, Routes.EditTransaction, argument: transaction),
          color: backgroundColorTo
        ),
        SlideAction(
          child: const Icon(Icons.delete),
          onTap: () => _delete(context, transaction),
          color: backgroundColorTo
        )
      ],
      child: Material(
        color: elevatedBackgroundColor,
        elevation: cardElevation,
        child: Padding(
          padding: const EdgeInsets.all(spacingUnit),
          child: IntrinsicHeight(
            child: Row(
              children: <Widget>[
                CategoryIndicator(category: transaction.category),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: spacingUnit),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                          if (transaction.category != null)
                            Padding(
                              padding: const EdgeInsets.only(bottom: spacingUnit / 2),
                              child: Text(
                                transaction.category.name,
                                style: Theme.of(context).textTheme.bodyText2,
                              ),
                            ),Text(transaction.store.name,
                                style: Theme.of(context).textTheme.bodyText1),
                          if (transaction.note?.isNotEmpty ?? false)
                              Padding(
                                padding: const EdgeInsets.only(top: spacingUnit / 2),
                                child: Text(
                                  transaction.note,
                                  style: Theme.of(context).textTheme.bodyText2,
                                ),
                              ),
                      ],
                    ),
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(right: spacingUnit * 0.5),
                  child: Text(
                    transaction.value.asCurrency(context),
                    style: Theme.of(context).textTheme.bodyText1.apply(
                        color: transaction.value < 0 ? expenseColor : incomeColor),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
