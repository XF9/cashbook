import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:intl/intl.dart';

import '../../database/database.dart';
import '../../generated/i18n.dart';
import '../../helper/extensions.dart';
import '../../helper/navigationHelper.dart';
import '../../provider/storeProvider.dart';
import '../../provider/transactionProvider.dart';
import '../base.dart';
import '../constants.dart';
import '../shared/categoryPickerFormField.dart';

enum TransactionType { income, expense }

class ManageTransaction extends StatefulWidget {
  final Transaction transaction;

  const ManageTransaction({Key key, this.transaction}) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<ManageTransaction> {
  final _formKey = GlobalKey<FormState>();
  final _storeController = TextEditingController();
  final _amountController = TextEditingController();
  final _dateController = TextEditingController();
  final _categoryController = TextEditingController();
  final _noteController = TextEditingController();
  var _setAsDefault = false;

  TransactionType _selectedType = TransactionType.expense;

  List<Store> _suggestions = [];

  void _populateForm(BuildContext context) {
    if (widget.transaction != null) {
      _storeController.text = widget.transaction.store.name;
      _amountController.text = widget.transaction.value.abs().toString();
      _dateController.text = widget.transaction.date.asShortDate(context);
      _selectedType = widget.transaction.value > 0
          ? TransactionType.income
          : TransactionType.expense;
      _categoryController.text = widget.transaction.category?.name ?? '';
      _noteController.text = widget.transaction.note;
    } else {
      _dateController.text = DateTime.now().asShortDate(context);
    }
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () => _populateForm(context));
    _loadStoreList();
  }

  Future _loadStoreList() async {
    final list = await StoreProvider().getStores();
    list.sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));
    setState(() {
      _suggestions = list;
    });
  }

  Iterable<Store> _filterStore(String value) {
    final searchString = value.toLowerCase();
    return _suggestions
        .where((store) => store.name.toLowerCase().contains(searchString));
  }

  @override
  Widget build(BuildContext context) {
    final currencySymbol = NumberFormat.simpleCurrency(
            locale: Localizations.localeOf(context).languageCode)
        .currencySymbol;

    Future _selectDate(BuildContext context) async {
      final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101),
      );

      if (picked != null) 
        _dateController.text = picked.asShortDate(context);
    }

    return BaseLayout(
        currentRoute: widget.transaction == null ? Routes.AddTransaction : Routes.EditTransaction,
        title: widget.transaction != null
            ? I18n.of(context).transactionsAddTitleEdit
            : I18n.of(context).transactionsAddTitle,
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: contentPaddingHorizontalOnly,
                child: DropdownButtonFormField<TransactionType>(
                  decoration: InputDecoration(
                      icon: const Icon(Icons.sync),
                      labelText: I18n.of(context).transactionsAddTypeLabel),
                  value: _selectedType,
                  items: [
                    DropdownMenuItem(
                        value: TransactionType.expense,
                        child: Text(
                            I18n.of(context).transactionsAddTypeOptionExpense)),
                    DropdownMenuItem(
                        value: TransactionType.income,
                        child: Text(
                            I18n.of(context).transactionsAddTypeOptionIncome)),
                  ],
                  onChanged: (newValue) {
                    setState(() {
                      _selectedType = newValue;
                    });
                  },
                ),
              ),
              Padding(
                padding: contentPaddingHorizontalOnly,
                child: TypeAheadFormField<Store>(
                  textFieldConfiguration: TextFieldConfiguration<String>(
                    controller: _storeController,
                    decoration: InputDecoration(
                      icon: const Icon(Icons.store),
                      hintText: _selectedType == TransactionType.income
                          ? I18n.of(context).transactionsAddStoreHintIncome
                          : I18n.of(context).transactionsAddStoreHintExpense,
                      labelText: _selectedType == TransactionType.income
                          ? I18n.of(context).transactionsAddStoreLabelIncome
                          : I18n.of(context).transactionsAddStoreLabelExpense,
                    ),
                  ),
                  suggestionsCallback: _filterStore,
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      title: Text(suggestion.name),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion) {
                    _storeController.text = suggestion.name;
                    if (suggestion.defaultCategory != null)
                      _categoryController.text =
                          suggestion.defaultCategory.name;
                  },
                  validator: (value) {
                    if (value == null || value == '')
                      return I18n.of(context)
                          .transactionsAddStoreValidationRequired;
                    return null;
                  },
                  hideOnEmpty: true,
                ),
              ),
              Padding(
                padding: contentPaddingHorizontalOnly,
                child: CategoryPickerFormField(
                    labelText: I18n.of(context).transactionsAddCategoryLabel, 
                    controller: _categoryController
                  ),
              ),
              Padding(
                padding: contentPaddingHorizontalOnly,
                child: FormField<bool>(
                  builder: (FormFieldState<dynamic> field) {
                    return Padding(
                      padding: const EdgeInsets.only(left: 16),
                      child: CheckboxListTile(
                        value: _setAsDefault,
                        onChanged: (val) {
                          setState(() {
                            _setAsDefault = val;
                          });
                        },
                        title: Text(
                          _selectedType == TransactionType.income 
                            ?I18n.of(context).transactionsAddCategorySetAsDefaultIncome
                            :I18n.of(context).transactionsAddCategorySetAsDefaultExpense,
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                        controlAffinity: ListTileControlAffinity.leading,
                        dense: true,
                      ),
                    );
                  },
                ),
              ),
              Padding(
                padding: contentPaddingHorizontalOnly,
                child: TextFormField(
                  decoration: InputDecoration(
                    icon: const Icon(Icons.attach_money),
                    hintText: _selectedType == TransactionType.income
                        ? I18n.of(context).transactionsAddAmountHintIncome
                        : I18n.of(context).transactionsAddAmountHintExpense,
                    labelText: I18n.of(context).transactionsAddAmountLabel,
                    suffixText: currencySymbol,
                  ),
                  validator: (value) {
                    if (value == '')
                      return I18n.of(context)
                          .transactionsAddAmountValidationRequired;
                    if (!value.isCurrency(context))
                      return I18n.of(context)
                          .transactionsAddAmountValidationFormat;
                    return null;
                  },
                  controller: _amountController,
                  keyboardType: TextInputType.number,
                ),
              ),
              Padding(
                padding: contentPaddingHorizontalOnly,
                child: TextFormField(
                  decoration: InputDecoration(
                    icon: const Icon(Icons.calendar_today),
                    labelText: I18n.of(context).transactionsAddDateLabel,
                  ),
                  controller: _dateController,
                  keyboardType: TextInputType.text,
                  readOnly: true,
                  onTap: () async {
                    await _selectDate(context);
                  },
                ),
              ),
              Padding(
                padding: contentPaddingHorizontalOnly,
                child: TextFormField(
                  decoration: InputDecoration(
                    icon: const Icon(Icons.description),
                    labelText: I18n.of(context).transactionsAddNoteLabel,
                  ),
                  controller: _noteController,
                  keyboardType: TextInputType.text,
                ),
              ),
              Padding(
                padding: contentPaddingHorizontalOnly,
                child: ButtonBar(
                  children: <Widget>[
                    FlatButton.icon(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          TransactionProvider.instance.saveTransaction(
                              date: _dateController.text.fromShortDate(context),
                              storename: _storeController.text,
                              value:
                                  _amountController.text.asCurrency(context) *
                                      (_selectedType == TransactionType.expense
                                          ? -1
                                          : 1),
                              id: widget.transaction?.id,
                              categoryName: _categoryController.text,
                              setCategoryAsDefault: _setAsDefault,
                              note: _noteController.text);
                          Navigator.pop(context);
                        }
                      },
                      label: Text(
                        I18n.of(context).transactionsAddSave,
                        style: Theme.of(context).textTheme.headline6.copyWith(color: primaryColor),
                      ),
                      icon: const Icon(Icons.save),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
  }
}
