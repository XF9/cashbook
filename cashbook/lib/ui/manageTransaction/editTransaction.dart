import 'package:flutter/widgets.dart';

import '../../database/database.dart';
import 'manageTransaction.dart';

class EditTransaction extends StatelessWidget {

  final Transaction transaction;

  const EditTransaction (this.transaction, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ManageTransaction(transaction: transaction);
  }
}