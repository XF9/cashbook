import 'package:flutter/widgets.dart';

import 'manageTransaction.dart';

class AddTransaction extends StatelessWidget {

  const AddTransaction ({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ManageTransaction();
  }
  
}