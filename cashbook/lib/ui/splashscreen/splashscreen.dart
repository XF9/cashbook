import 'package:cashflow/database/fileBase.dart';
import 'package:cashflow/helper/navigationHelper.dart';
import 'package:cashflow/ui/base.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SplashScreen extends StatefulWidget{
  const SplashScreen();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>{
  ErrorData _errorData;

  @override
  void initState(){
    super.initState();
    FileBase()
      .load()
      .then((data){
        NavigationHelper.removeFromHistory(context);
        
        if(data.confirmedTos == null)
          NavigationHelper.navigateTo(context, Routes.About);
        else
          NavigationHelper.navigateTo(context, Routes.DashBoard);
      });
  }

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      hideHeader: true,
      errorData: _errorData,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Padding(
                padding: EdgeInsets.all(contentSpacing),
                child: CircularProgressIndicator(),
              ),
              Text('Loading ..')
            ],
          )
        ],
      ),
    );
  }
}