import 'package:flutter/material.dart';

import '../../generated/i18n.dart';
import '../../helper/navigationHelper.dart';
import '../drawer/drawerHead.dart';

class MainDrawer extends StatelessWidget {
  final String currentRoute;

  const MainDrawer({Key key, this.currentRoute}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          const DrawerHead(),
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                ListTile(
                  title: Text(I18n.of(context).pagesDashboard),
                  selected: currentRoute.startsWith(Routes.DashBoard),
                  leading: const Icon(Icons.home),
                  onTap: () {
                    NavigationHelper.navigateTo(context, Routes.DashBoard, closeDrawer: true);
                  },
                ),
                ListTile(
                  title: Text(I18n.of(context).pagesAdd),
                  selected: currentRoute.startsWith(Routes.AddTransaction),
                  leading: const Icon(Icons.add),
                  onTap: () {
                    NavigationHelper.navigateTo(context, Routes.AddTransaction, closeDrawer: true);
                  },
                ),
                const Divider(),
                ListTile(
                  title: Text(I18n.of(context).pagesTransactionList),
                  selected: currentRoute.startsWith(Routes.TransactionList),
                  leading: const Icon(Icons.list),
                  onTap: () {
                    NavigationHelper.navigateTo(context, Routes.TransactionList, closeDrawer: true);
                  },
                ),
                ListTile(
                  title: Text(I18n.of(context).pagesEvaluation),
                  selected: currentRoute.startsWith(Routes.Evaluation),
                  leading: const Icon(Icons.show_chart),
                  onTap: () {
                    NavigationHelper.navigateTo(context, Routes.Evaluation, closeDrawer: true);
                  },
                ),
                const Divider(),
                ListTile(
                  title: Text(I18n.of(context).settingsStoresName),
                  selected: currentRoute.startsWith(Routes.SettingsStores),
                  leading: const Icon(Icons.store),
                  onTap: () {
                    NavigationHelper.navigateTo(context, Routes.SettingsStores, closeDrawer: true);
                  },
                ),
                ListTile(
                  title: Text(I18n.of(context).settingsCategoriesName),
                  selected: currentRoute.startsWith(Routes.SettingsCategories),
                  leading: const Icon(Icons.label),
                  onTap: () {
                    NavigationHelper.navigateTo(context, Routes.SettingsCategories, closeDrawer: true);
                  },
                ),
                const Divider(),
                ListTile(
                  title: Text(I18n.of(context).settingsBackupName),
                  selected: currentRoute.startsWith(Routes.SettingsBackup),
                  leading: const Icon(Icons.backup),
                  onTap: () {
                    NavigationHelper.navigateTo(context, Routes.SettingsBackup, closeDrawer: true);
                  },
                ),
                ListTile(
                  title: Text(I18n.of(context).aboutTitle),
                  selected: currentRoute.startsWith(Routes.About),
                  leading: const Icon(Icons.info),
                  onTap: () {
                    NavigationHelper.navigateTo(context, Routes.About, closeDrawer: true);
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
