import 'package:cashflow/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DrawerHead extends StatelessWidget {

  const DrawerHead({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      margin: const EdgeInsets.all(0),
      padding: const EdgeInsets.all(0),
      child: Container(
        width: 10000,
        child: CustomPaint(
          painter: LogoPainter(context),
        ),
      )
    );
  }
}

class LogoPainter extends CustomPainter {

  TextPainter textCash;
  TextPainter textFlow;
  TextPainter textMeter;
  TextPainter textVersion;

  LogoPainter(BuildContext context){
    textCash = TextPainter(
        text: TextSpan(
          text: 'Cash',
          style: Theme.of(context).textTheme.headline4
        ),
        textAlign: TextAlign.right,
        textDirection: TextDirection.ltr);
    textCash.layout();

    textFlow = TextPainter(
        text: TextSpan(
          text: 'Flow',
          style: Theme.of(context).textTheme.headline4.copyWith(color: primaryColor)
        ),
        textAlign: TextAlign.right,
        textDirection: TextDirection.ltr);
    textFlow.layout();

    textMeter = TextPainter(
        text: const TextSpan(
          text: 'Meter'
        ),
        textAlign: TextAlign.right,
        textDirection: TextDirection.ltr);
    textMeter.layout();

    textVersion = TextPainter(
        text: TextSpan(
          text: currentVersion,
          style: Theme.of(context).textTheme.bodyText2.copyWith(color: onBackgroundColor, fontSize: 12)
        ),
        textAlign: TextAlign.right,
        textDirection: TextDirection.ltr);
    textVersion.layout();
  }

  @override
  void paint(Canvas canvas, Size size) {
      final centerX = size.width / 2;
      final centerY = size.height / 2;
      final line1start = centerX - (textCash.width + textFlow.width) / 2;
      final line1end = centerX + (textCash.width + textFlow.width) / 2;
      final line2start = centerX - (textMeter.width) / 2;
      final lineY = centerY + textMeter.height / 2;
      final linepaint = Paint()
        ..color = Colors.white
        ..strokeWidth = 1.2;

      textCash.paint(canvas, Offset(line1start, centerY - textCash.height));
      textFlow.paint(canvas, Offset(line1start + textFlow.width, centerY - textCash.height));
      textMeter.paint(canvas, Offset(line2start, centerY));
      canvas.drawLine(Offset(line1start, lineY), Offset(line2start - 5, lineY), linepaint);
      canvas.drawLine(Offset(line2start + textMeter.width + 5, lineY), Offset(line1end, lineY), linepaint);
      textVersion.paint(canvas, Offset(size.width - textVersion.width - 10, size.height - textVersion.height - 5));
    }
  
    @override
    bool shouldRepaint(CustomPainter oldDelegate) {
      return false;
  }
}