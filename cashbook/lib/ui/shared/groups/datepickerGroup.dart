import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../helper/extensions.dart';

class DatepickerGroup extends StatefulWidget {
  final DateTime initialValue;
  final void Function(DateTime) onChange;
  final String label;
  final DateTime from;
  final DateTime to;

  const DatepickerGroup(this.label, this.initialValue, this.onChange,
      {Key key, this.from, this.to})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<DatepickerGroup> {
  final _dateController = TextEditingController();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _dateController.text.fromShortDate(context),
        firstDate: widget.from ?? DateTime(2010),
        lastDate: widget.to ?? DateTime(2100));

    if (picked != null) 
      widget.onChange(picked);
  }

  void _updateControllerText(BuildContext context) {
    _dateController.text = widget.initialValue.asShortDate(context);
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () => _updateControllerText(context));
  }

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    _updateControllerText(context);
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        labelText: widget.label,
      ),
      controller: _dateController,
      keyboardType: TextInputType.text,
      readOnly: true,
      onTap: () async {
        await _selectDate(context);
      },
    );
  }
}
