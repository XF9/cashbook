import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SelectGroupItem<T>{
  T value;
  String label;
  String Function(T) labelFunc;
  T Function() onPress;

  SelectGroupItem({this.value, this.label, this.labelFunc, this.onPress});

  String getLabel(){
    return labelFunc != null
      ? labelFunc(value)
      : label;
  }
}

class SelectGroup<T> extends StatefulWidget{

  final List<SelectGroupItem<T>> buttons;
  final T selectedValue;
  final void Function(T) onChange;
  final String label;

  const SelectGroup(this.label, this.selectedValue, this.buttons, this.onChange, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _State<T>();
}

class _State<T> extends State<SelectGroup<T>> {

  T currentValue;

  @override
  void initState() {
    super.initState();
    currentValue = widget.selectedValue;
  }

  @override
  void setState(void Function() func) {
    super.setState(func);
    widget.onChange(currentValue);
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<T>(
      decoration: InputDecoration(
        labelText: widget.label
      ),
      isDense: true,
      value: currentValue,
      hint: Text(widget.label),
      items: widget.buttons.map((data) => DropdownMenuItem(
        value: data.value,
        child: Text(data.getLabel()),
      )).toList(),
      onChanged: (T newValue) => setState(() { currentValue = newValue; }),
    );
  }
}