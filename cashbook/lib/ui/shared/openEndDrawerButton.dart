import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class OpenEndDrawerButton extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) => IconButton(
        icon: const Icon(Icons.tune),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    );
  }
  
}