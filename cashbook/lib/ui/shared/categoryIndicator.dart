import 'package:flutter/widgets.dart';

import '../../database/database.dart';
import '../constants.dart';

class CategoryIndicator extends StatelessWidget {
  final Category category;

  const CategoryIndicator(
      {Key key, @required this.category})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(5)),
        color: category != null ? category.color : noCategoryColor,
      ),
      width: 3,
    );
  }
}
