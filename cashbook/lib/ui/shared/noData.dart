import 'package:cashflow/generated/i18n.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NoData extends StatelessWidget {
  
  const NoData({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: contentPadding,
      child: Center(child: Text(
        I18n.of(context).noData,
        style: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.bold)
      )),
    );
  }
}
