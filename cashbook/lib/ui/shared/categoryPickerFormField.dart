import 'package:cashflow/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import '../../database/database.dart';
import '../../provider/categoryProvider.dart';
import 'categoryIndicator.dart';

class CategoryPickerFormField extends StatefulWidget{

  final String labelText;
  final Category selectedCategory;
  final TextEditingController controller;

  const CategoryPickerFormField({Key key, @required this.labelText, this.selectedCategory, @required this.controller}) : super(key: key);

  @override
  State<StatefulWidget> createState() => CategoryPickerFormFieldState();
}

class CategoryPickerFormFieldState extends State<CategoryPickerFormField>{

  List<Category> _categories = [];

  Future _fetchList() async {
    final list= await CategoryProvider.instance.getCategories();
    list.sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));

    if(widget.selectedCategory != null){
        widget.controller.text = widget.selectedCategory.name;
    }

    setState(() {
      _categories = list;
    });
  }

  @override void initState(){
    super.initState();
    _fetchList();
  }

  Iterable<Category> _filterCategory(String value){
    final searchString = value.toLowerCase();
    return _categories.where((category) => category.name.toLowerCase().contains(searchString));
  }

  @override
  Widget build(BuildContext context) {
    return TypeAheadFormField<Category>(
      textFieldConfiguration: TextFieldConfiguration<String>(
        decoration: InputDecoration(
          icon: const Icon(Icons.label),
          labelText: widget.labelText,
        ),
        controller: widget.controller,
      ),
      onSuggestionSelected: (category) => 
        widget.controller.text = category.name,
      itemBuilder: (_, category) =>
        ListTile(
          leading: Padding(
            padding: const EdgeInsets.all(spacingUnit),
            child: CategoryIndicator(category: category),
          ),
          title: Text(category.name),
        ),
      suggestionsCallback: _filterCategory,
      transitionBuilder: (context, suggestionsBox, controller) {
        return suggestionsBox;
      },
      hideOnEmpty: true,
    );
  }
}