import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../generated/i18n.dart';
import '../constants.dart';

class FilterHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: spacingUnit),
                child: Icon(
                  Icons.tune,
                  size: Theme.of(context).textTheme.headline4.fontSize,
                ),
              ),
              Text(
                I18n.of(context).filterTitle,
                style: Theme.of(context).textTheme.headline4,
              ),
            ],
          ),
          Text(
            I18n.of(context).filterSubtitle,
            style: Theme.of(context).textTheme.bodyText2,
          )
        ],
      ),
    );
  }
}
