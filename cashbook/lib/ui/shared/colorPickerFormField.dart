import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

import '../../generated/i18n.dart';
import '../constants.dart';

class ColorPickerFormField extends FormField<Color>{
  ColorPickerFormField({
    FormFieldSetter<Color> onSaved,
    FormFieldValidator<Color> validator,
    Color initialValue = primaryColor,
    Widget icon,
    @required BuildContext context,
    @required Function(Color newColor) onChange
  }) : super(
    onSaved: onSaved,
    validator: validator,
    initialValue: initialValue,
    autovalidateMode: AutovalidateMode.disabled,
    builder: (FormFieldState<Color> state) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: spacingUnit),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if(icon != null)
              Padding(
                padding: const EdgeInsets.only(right: 18),
                child: icon,
              ),
            Expanded(
              child: InkWell(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(3)),
                    color: state.value
                  ),
                  height: 50,
                ),
                onTap: () => {
                  _pickColor(context, state, onChange)
                },
              ),
            )
          ],
        ),
      );
    }
  );

  static void _pickColor(BuildContext context, FormFieldState<Color> state, Function(Color newColor) onChange){
    var currentColor = state.value;
    showDialog<Color>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(I18n.of(context).colorPickerName),
          content: SingleChildScrollView(
            child: SlidePicker(
              pickerColor: currentColor,
              onColorChanged: (changed) => {currentColor = changed},
              paletteType: PaletteType.hsv,
              enableAlpha: false,
              displayThumbColor: true,
              showLabel: false,
              showIndicator: true,
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                I18n.of(context).colorPickerCancel,
                  style: Theme.of(context).textTheme.button
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text(
                I18n.of(context).colorPickerSave,
                  style: Theme.of(context).textTheme.button.copyWith(color: primaryColor)
              ),
              onPressed: () {
                state.didChange(currentColor);
                onChange(currentColor);
                Navigator.pop(context);
              },
            ),
          ],
        );
      }
    );
  }
}