import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../constants.dart';

class EndDrawerFilter extends StatelessWidget{

  final List<Widget> children;

  const EndDrawerFilter({Key key, this.children}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Padding(
        padding: const EdgeInsets.all(spacingUnit),
        child: ListView.builder(
          itemBuilder: (BuildContext context, int index){
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: spacingUnit * .1),
              child: children[index],
            );
          },
          itemCount: children.length,
        ),
      )
    );
  }
}