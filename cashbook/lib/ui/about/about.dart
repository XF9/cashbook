import 'package:cashflow/database/fileBase.dart';
import 'package:cashflow/generated/i18n.dart';
import 'package:cashflow/helper/extensions.dart';
import 'package:cashflow/helper/navigationHelper.dart';
import 'package:cashflow/ui/base.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:cashflow/ui/drawer/drawerHead.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';

class About extends StatefulWidget{
  const About();
  @override
    _AboutState createState() => _AboutState();
}

class _AboutState extends State<About>{

  bool _isChecked = false;
  bool _loading = true;
  DateTime _confirmationDate;

  Future _sendMail(BuildContext context) async{
    if(await canLaunch(feedbackAdressUrl))
      launch(feedbackAdressUrl);
    else
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(I18n.of(context).cantSendMail(feedbackAdress))
        )
      );
  }

  Future _loadData() async{
    final data = await FileBase().load();
    setState((){
      _confirmationDate = data.confirmedTos;
      _loading = false;
    });
  }

  Future _handleGo() async{
    if(_confirmationDate == null){
      if(!_isChecked)
        return;
      await FileBase().saveConfirmedTosDate(DateTime.now());
    }
    NavigationHelper.navigateTo(context, Routes.DashBoard);
  }

  @override
  void initState(){
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      hideHeader: true,
      child: ListView(
        children: [
          const DrawerHead(),
          Padding(
            padding: const EdgeInsets.all(contentSpacing),
            child: Text(
              I18n.of(context).aboutGreetings,
              style: Theme.of(context).textTheme.bodyText1
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: I18n.of(context).aboutFeedback,
                    style: Theme.of(context).textTheme.bodyText1
                  ),
                  TextSpan(
                    text: feedbackAdress,
                    style: Theme.of(context).textTheme.bodyText1.copyWith(color: primaryColor),
                    recognizer: TapGestureRecognizer()
                    ..onTap = () => _sendMail(context),
                  )
                ]
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(contentSpacing),
            child: Text(
              I18n.of(context).aboutStoredData,
              style: Theme.of(context).textTheme.bodyText1
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
            child: Text(
              I18n.of(context).aboutLiability,
              style: Theme.of(context).textTheme.bodyText1
            ),
          ),
          if(_loading)
            const Padding(
              padding: EdgeInsets.all(contentSpacing),
              child: CircularProgressIndicator(),
            ),
          if(!_loading && _confirmationDate == null)
            Padding(
              padding: const EdgeInsets.all(contentSpacing),
              child: CheckboxListTile(
                onChanged: (newValue) => setState(() => _isChecked = newValue),
                value: _isChecked,
                title: Text(I18n.of(context).aboutConfirm),
              ),
            ),
          if(!_loading && _confirmationDate != null)
            Padding(
              padding: const EdgeInsets.all(contentSpacing),
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: I18n.of(context).aboutConfirmedAt,
                      style: Theme.of(context).textTheme.bodyText2
                    ),
                    TextSpan(
                      text: _confirmationDate.asDay(context),
                      style: Theme.of(context).textTheme.bodyText2
                    )
                  ]
                )
              )
            ),
          if(!_loading)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
              child: ButtonBar(
                children: [
                  RaisedButton(
                    color: primaryColor,
                    disabledColor: subtlColor,
                    child: Text(I18n.of(context).aboutCloseButton),
                    onPressed: _confirmationDate != null || _isChecked
                      ? _handleGo
                      : null
                  )
                ],
              ),
            ),
        ],
      )
    );
  }

}