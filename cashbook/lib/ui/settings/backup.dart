import 'dart:io';

import 'package:cashflow/database/database.dart';
import 'package:cashflow/database/fileBase.dart';
import 'package:cashflow/generated/i18n.dart';
import 'package:cashflow/helper/navigationHelper.dart';
import 'package:cashflow/helper/extensions.dart';
import 'package:cashflow/ui/base.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';

class Backup extends StatelessWidget{
  const Backup({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      title: I18n.of(context).settingsBackupName,
      currentRoute: Routes.SettingsBackup,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(contentSpacing),
            child: Text(
              I18n.of(context).settingsBackupDescription,
              style: Theme.of(context).textTheme.bodyText1,
            )
          ),
          ButtonBar(
            alignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              FlatButton.icon(
                onPressed: () async {
                  final file = await FilePicker.getFile();
                  
                  if(file == null)
                    return;

                  final lines = file.readAsLinesSync();
                  await Database().import(lines);
                },
                label: Text(
                  I18n.of(context).settingsBackupButtonsRestore,
                  style: Theme.of(context).textTheme.headline6.copyWith(color: primaryColor),
                ),
                icon: const Icon(Icons.cloud_download),
              ),
              FlatButton.icon(
                onPressed: () async {
                  final backupLines = await Database().export();
                  final backup = backupLines.fold<String>('', (prev, curr) => prev == '' ? curr :  prev + '\n' + curr);
                  final directory = await getExternalStorageDirectory();
                  final now = DateTime.now();
                  final file = File('${directory.path}/cashflow-${now.year}-${now.month}-${now.day}.txt');
                  if(file.existsSync())
                    file.deleteSync();
                  file.createSync();
                  file.writeAsStringSync(backup, flush: true);

                  Share.shareFiles([file.path], subject: 'Cashflow Export ${now.asShortDate(context)}', text: 'This is your backup file from cashflow.');

                  FileBase().saveNextBackupReminder(DateTime.now().add(const Duration(days: 30)));
                },
                label: Text(
                  I18n.of(context).settingsBackupButtonsBackup,
                  style: Theme.of(context).textTheme.headline6.copyWith(color: primaryColor),
                ),
                icon: const Icon(Icons.cloud_upload),
              ),
            ],
          )
        ],
      )
    );
  }
}