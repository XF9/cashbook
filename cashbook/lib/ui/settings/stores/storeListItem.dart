import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../database/database.dart';
import '../../../helper/navigationHelper.dart';

class StoreListItem extends StatelessWidget{

  final Store store;

  const StoreListItem({Key key, @required this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(store.name),
      trailing: const Icon(Icons.edit),
      onTap: () => NavigationHelper.navigateTo(context, Routes.SettingsStoresEdit, argument: store)
    );
  }  
}