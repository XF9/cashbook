import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../database/database.dart';
import '../../../generated/i18n.dart';
import '../../../helper/navigationHelper.dart';
import '../../../provider/storeProvider.dart';
import '../../base.dart';
import '../../shared/noData.dart';
import 'storeListItem.dart';

class StoreList extends StatefulWidget{
  const StoreList({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => StoreListState();
}

class StoreListState extends State<StoreList>{

  List<Store> _stores;

  @override 
  void initState() {
    super.initState();
    StoreProvider.instance.startObserving(_fetchData);
    _stores = <Store>[];
    _fetchData();
  }

  @override
  void dispose(){
    super.dispose();
    StoreProvider.instance.endObserving(_fetchData);
  }

  Future _fetchData() async{
    final data = await StoreProvider.instance.getStores();
    setState(() {
      _stores = data;
    });
  }

  @override
  Widget build(BuildContext context) {

    _stores.sort((a, b) => a.name.compareTo(b.name));

    return BaseLayout(
      title: I18n.of(context).settingsStoresName,
      currentRoute: Routes.SettingsStores,
      child: _stores.isEmpty
        ? ListView(
          children: const [ NoData() ]
        )
        : ListView.separated(
          separatorBuilder: (_, __) => const Divider(),
          itemBuilder: (_, index) => StoreListItem(store: _stores[index]),
          itemCount: _stores.length,
        ),
      appBarActions: <Widget>[
        IconButton(
          icon: const Icon(Icons.add), 
          onPressed: () => NavigationHelper.navigateTo(context, Routes.SettingsStoresAdd)          
        )
      ],
    );
  }
  
}