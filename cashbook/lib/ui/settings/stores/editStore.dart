import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../database/database.dart';
import '../../../generated/i18n.dart';
import '../../../helper/navigationHelper.dart';
import '../../../provider/storeProvider.dart';
import '../../base.dart';
import '../../constants.dart';
import '../../shared/categoryPickerFormField.dart';

enum Options { merge }

class EditStore extends StatefulWidget{

  final Store store;

  const EditStore({Key key, this.store}) : super(key: key);

  @override
  State<StatefulWidget> createState() => EditStoreState();
}

class EditStoreState extends State<EditStore>{

  final _storenameController = TextEditingController();
  final _categoryController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  
  @override void initState(){
    super.initState();
    _storenameController.text = widget.store?.name;
  }

  @override void dispose(){
    super.dispose();
    _storenameController.dispose();
    _categoryController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      title: widget.store?.id != null 
        ? I18n.of(context).settingsStoresEdit 
        : I18n.of(context).settingsStoresAdd,
      currentRoute: widget.store?.id != null 
        ? Routes.SettingsStoresEdit
        : Routes.SettingsStoresAdd,
      child: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
              child: TextFormField(
                decoration: InputDecoration(
                  icon: const Icon(Icons.store),
                  labelText: I18n.of(context).settingsStoresFormName,
                ),
                controller: _storenameController,
                keyboardType: TextInputType.text,
                validator: (value){
                  if(value == '')
                    return I18n.of(context).settingsStoresFormNameRequired;
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
              child: CategoryPickerFormField(
                labelText: I18n.of(context).settingsStoresFormDefaultCategory,
                selectedCategory: widget.store?.defaultCategory,
                controller: _categoryController,
              )
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: spacingUnit),
              child: ButtonBar(
                children: <Widget>[
                  RaisedButton(
                    child: Text(I18n.of(context).settingsStoresFormSave),
                    onPressed: () {
                      if(_formKey.currentState.validate()){
                        StoreProvider.instance.saveStore(widget.store?.id, _storenameController.text, _categoryController.text);
                        Navigator.pop(context);
                      }
                    },
                    color: primaryColor,
                  )
                ],
              ),
            )
          ]
        ),
      ),
      appBarActions: [
        if(widget.store?.id != null)
          PopupMenuButton<Options>(
            icon: Icon(
              Icons.more_vert,
              color: Theme.of(context).colorScheme.secondary
            ),
            itemBuilder: (BuildContext context) => [
              PopupMenuItem(
                value: Options.merge,
                child: Row(
                    children: <Widget>[
                      const Padding(
                        padding: EdgeInsets.only(right: spacingUnit),
                        child: Icon(
                          Icons.merge_type_rounded,
                          color: dangerColor,
                        ),
                      ),
                      Text(
                        I18n.of(context).settingsStoresMergeTitle,
                        style: Theme.of(context).textTheme.button.copyWith(color: dangerColor)
                      )
                    ],
                  )
              )
            ],
            onSelected: (value) => {
              if(value == Options.merge)
                NavigationHelper.navigateTo(context, Routes.SettingsStoresMerge, argument: widget.store)
            },
          ),
      ],
    );
  }
}