import 'package:cashflow/database/database.dart';
import 'package:cashflow/generated/i18n.dart';
import 'package:cashflow/helper/navigationHelper.dart';
import 'package:cashflow/provider/storeProvider.dart';
import 'package:cashflow/ui/base.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class MergeStore extends StatefulWidget{

  final Store toBeDeleted;

  const MergeStore({Key key, @required this.toBeDeleted}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<MergeStore>{

  List<Store> _stores = [];
  Store _toBeMergedInto;

  @override
  void initState() {
    super.initState();
    _loadStores();
  }

  Future _loadStores() async{ 
    final stores = await StoreProvider.instance.getStores();
    setState(() {
      _stores = stores
        .where((element) => element.id != widget.toBeDeleted.id)
        .toList();
    });
  }

  void _reset(){
    setState(() {
      _toBeMergedInto = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _submitMerge = ()async{
      if(_toBeMergedInto == null)
        return null;
      await StoreProvider.instance.mergeStore(toBeDeleted: widget.toBeDeleted, toBeMergedInto: _toBeMergedInto);
      // We need to go back 2 steps since we need to close the current view and the edit view of the now deleted store
      NavigationHelper.removeFromHistory(context);
      NavigationHelper.removeFromHistory(context);
    };

    return BaseLayout(
      title: I18n.of(context).settingsStoresMergeTitle,
      currentRoute: Routes.SettingsStoresEdit,
      child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(contentSpacing),
            child: Text(
              I18n.of(context).settingsStoresMergeWarning,
              style: Theme.of(context).textTheme.bodyText1.copyWith(color: dangerColor, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
            child: Text(
              I18n.of(context).settingsStoresMergeDescription(widget.toBeDeleted.name),
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          if(_toBeMergedInto == null)
            Padding(
              padding: const EdgeInsets.all(contentSpacing),
              child: TypeAheadField<Store>(
                textFieldConfiguration: TextFieldConfiguration<String>(
                  decoration: InputDecoration(
                    icon: const Icon(Icons.store),
                    labelText: I18n.of(context).settingsStoresMergeSearch,
                  ),
                ),
                itemBuilder: (context, suggestion){
                  return ListTile(
                    title: Text(suggestion.name),
                  );
                },
                onSuggestionSelected: (suggestion){
                  setState(() {
                    _toBeMergedInto = suggestion;
                  });
                },
                suggestionsCallback: (searchString) => _stores
                  .where((store) => store
                    .name
                    .toLowerCase()
                    .contains(searchString.toLowerCase())
                  ),
                transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
              ),
            ),
          if(_toBeMergedInto != null)
            Padding(
              padding: const EdgeInsets.symmetric(vertical: contentSpacing),
              child: ListTile(
                title: Text(_toBeMergedInto.name),
                onTap: _reset,
                leading: Checkbox(
                  value: true,
                  onChanged: (_) => _reset(),
                ),
              ),
            ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
            child: ButtonBar(
                children: [
                  RaisedButton(
                    child: Text(I18n.of(context).settingsStoresMergeSubmit),
                    color: dangerColor,
                    disabledColor: disabledColor,
                    onPressed: _toBeMergedInto == null
                      ? null
                      : _submitMerge
                  )
                ],
              ),
          )
        ],
      ),
    );
  }
  
}