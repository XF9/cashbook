import 'package:cashflow/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../database/database.dart';
import '../../../helper/navigationHelper.dart';
import '../../shared/categoryIndicator.dart';

class CategoryListItem extends StatelessWidget{

  final Category category;

  const CategoryListItem({Key key, @required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Padding(
        padding: const EdgeInsets.all(spacingUnit),
        child: CategoryIndicator(category: category),
      ),
      trailing: const Icon(Icons.edit),
      title: Text(category.name),
      onTap: () => NavigationHelper.navigateTo(context, Routes.SettingsCategoriesEdit, argument: category),
    );
  }
}