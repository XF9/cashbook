import 'package:cashflow/database/database.dart' as db;
import 'package:cashflow/generated/i18n.dart';
import 'package:cashflow/helper/navigationHelper.dart';
import 'package:cashflow/provider/categoryProvider.dart';
import 'package:cashflow/ui/base.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class MergeCategory extends StatefulWidget{

  final db.Category toBeDeleted;

  const MergeCategory({Key key, @required this.toBeDeleted}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<MergeCategory>{

  List<db.Category> _categories = [];
  db.Category _toBeMergedInto;

  @override
  void initState() {
    super.initState();
    _loadCategories();
  }

  Future _loadCategories() async{ 
    final categories = await CategoryProvider.instance.getCategories();
    setState(() {
      _categories = categories
        .where((element) => element.id != widget.toBeDeleted.id)
        .toList();
    });
  }

  void _reset(){
    setState(() {
      _toBeMergedInto = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _submitMerge = ()async{
      if(_toBeMergedInto == null)
        return null;
      await CategoryProvider.instance.mergeCategory(toBeDeleted: widget.toBeDeleted, toBeMergedInto: _toBeMergedInto);
      // We need to go back 2 steps since we need to close the current view and the edit view of the now deleted category
      NavigationHelper.removeFromHistory(context);
      NavigationHelper.removeFromHistory(context);
    };

    return BaseLayout(
      title: I18n.of(context).settingsCategoriesMergeTitle,
      currentRoute: Routes.SettingsStoresEdit,
      child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(contentSpacing),
            child: Text(
              I18n.of(context).settingsCategoriesMergeWarning,
              style: Theme.of(context).textTheme.bodyText1.copyWith(color: dangerColor, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
            child: Text(
              I18n.of(context).settingsCategoriesMergeDescription(widget.toBeDeleted.name),
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          if(_toBeMergedInto == null)
            Padding(
              padding: const EdgeInsets.all(contentSpacing),
              child: TypeAheadField<db.Category>(
                textFieldConfiguration: TextFieldConfiguration<String>(
                  decoration: InputDecoration(
                    icon: const Icon(Icons.store),
                    labelText: I18n.of(context).settingsCategoriesMergeSearch,
                  ),
                ),
                itemBuilder: (context, suggestion){
                  return ListTile(
                    title: Text(suggestion.name),
                  );
                },
                onSuggestionSelected: (suggestion){
                  setState(() {
                    _toBeMergedInto = suggestion;
                  });
                },
                suggestionsCallback: (searchString) => _categories
                  .where((category) => category
                    .name
                    .toLowerCase()
                    .contains(searchString.toLowerCase())
                  ),
                transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
              ),
            ),
          if(_toBeMergedInto != null)
            Padding(
              padding: const EdgeInsets.symmetric(vertical: contentSpacing),
              child: ListTile(
                title: Text(_toBeMergedInto.name),
                onTap: _reset,
                leading: Checkbox(
                  value: true,
                  onChanged: (_) => _reset(),
                ),
              ),
            ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
            child: ButtonBar(
                children: [
                  RaisedButton(
                    child: Text(I18n.of(context).settingsCategoriesMergeSubmit),
                    color: dangerColor,
                    disabledColor: disabledColor,
                    onPressed: _toBeMergedInto == null
                      ? null
                      : _submitMerge
                  )
                ],
              ),
          )
        ],
      ),
    );
  }
}