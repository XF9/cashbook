import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../database/database.dart';
import '../../../generated/i18n.dart';
import '../../../helper/navigationHelper.dart';
import '../../../provider/categoryProvider.dart';
import '../../base.dart';
import '../../shared/noData.dart';
import 'categoryListItem.dart';

class CategoryList extends StatefulWidget {
  const CategoryList({Key key}) : super(key: key);

  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList>{

  List<Category> categories = [];

  _CategoryListState(){
    _refreshData();
  }

  @override 
  void initState() {
    super.initState();
    CategoryProvider.instance.startObserving(_refreshData);
    _refreshData();
  }

  @override
  void dispose(){
    super.dispose();
    CategoryProvider.instance.endObserving(_refreshData);
  }

  Future _refreshData() async {
    final data = await CategoryProvider.instance.getCategories();
    if(mounted)
      setState(() {
        categories = data;
      });
  }

  @override
  Widget build(BuildContext context) {

    categories.sort((a, b) => a.name.compareTo(b.name));

    return BaseLayout(
      title: I18n.of(context).settingsCategoriesName,
      currentRoute: Routes.SettingsCategories,
      appBarActions: <Widget>[
        IconButton(
          icon: const Icon(Icons.add), 
          onPressed: () => NavigationHelper.navigateTo(context, Routes.SettingsCategoriesAdd)
        )
      ],
      child: categories.isEmpty
        ? ListView(
          children: const [ NoData() ]
        )
        : ListView.separated(
          separatorBuilder: (_, __) => const Divider(),
          itemBuilder: (_, index) => CategoryListItem(category: categories[index]),
          itemCount: categories.length
        ),
    );
  }
}