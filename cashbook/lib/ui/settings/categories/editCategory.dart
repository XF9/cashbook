import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../database/database.dart';
import '../../../generated/i18n.dart';
import '../../../helper/navigationHelper.dart';
import '../../../provider/categoryProvider.dart';
import '../../base.dart';
import '../../constants.dart';
import '../../shared/colorPickerFormField.dart';

enum Options { merge }

class EditCategory extends StatefulWidget {

  final Category category;

  const EditCategory({Key key, this.category}) : super(key: key);

  @override
  _EditCategoryState createState() => _EditCategoryState();
}

class _EditCategoryState extends State<EditCategory>{

  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  Color color;

  @override void initState(){
    super.initState();
    nameController.text = widget.category?.name ?? '';
    color = widget.category?.color ?? Colors.white;
  }

  @override void dispose(){
    super.dispose();
    nameController.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return BaseLayout(
      title: widget.category != null 
        ? I18n.of(context).settingsCategoriesEdit 
        : I18n.of(context).settingsCategoriesAdd,
      currentRoute: widget.category != null 
        ? Routes.SettingsCategoriesEdit
        : Routes.SettingsCategoriesAdd,
      child: Form(
        key: formKey,
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
              child: TextFormField(
                decoration: InputDecoration(
                  icon: const Icon(Icons.label),
                  labelText: I18n.of(context).settingsStoresFormName,
                ),
                validator: (value){
                  if(value == '')
                    return I18n.of(context).settingsCategoriesFormNameRequired;
                  return null;
                },
                controller: nameController
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: contentSpacing),
              child: ColorPickerFormField(
                icon: const Icon(Icons.color_lens),
                context: context,
                initialValue: color,
                onChange: (newColor) => setState(() {
                  color = newColor;
                }),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: spacingUnit),
              child: ButtonBar(
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {
                      if (formKey.currentState.validate()) {
                        CategoryProvider.instance.saveCategory(id: widget.category?.id, name: nameController.text, color: color);
                        Navigator.pop(context);
                      }
                    },
                    child: Text(I18n.of(context).settingsCategoriesFormSave),
                    color: primaryColor,
                  ),
                ],
              ),
            )
          ],
        )
      ),
      appBarActions: [
        if(widget.category?.id != null)
          PopupMenuButton<Options>(
            icon: Icon(
              Icons.more_vert,
              color: Theme.of(context).colorScheme.secondary
            ),
            itemBuilder: (BuildContext context) => [
              PopupMenuItem(
                value: Options.merge,
                child: Row(
                    children: <Widget>[
                      const Padding(
                        padding: EdgeInsets.only(right: spacingUnit),
                        child: Icon(
                          Icons.merge_type_rounded,
                          color: dangerColor,
                        ),
                      ),
                      Text(
                        I18n.of(context).settingsCategoriesMergeTitle,
                        style: Theme.of(context).textTheme.button.copyWith(color: dangerColor)
                      )
                    ],
                  )
              )
            ],
            onSelected: (value) => {
              if(value == Options.merge)
                NavigationHelper.navigateTo(context, Routes.SettingsCategoriesMerge, argument: widget.category)
            },
          ),
      ],

    );
  }

}