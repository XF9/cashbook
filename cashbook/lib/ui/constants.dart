import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

const double spacingUnit = 10;
const double contentSpacing = spacingUnit * 2;
const double diagramBarHeight = 30;
const double cardElevation = 5;

const Color primaryColor = Color(0xFFFF7A00);

const Color backgroundColor = Color(0xFF121212);
const Color backgroundColorTo = Color(0xFF222226);

const Color elevatedBackgroundColor = Color(0xFF1A1A1A);

const Color onBackgroundColor = Color(0xFFCCCCCC);
const Color onBackgroundColor2 = Color(0xFF666666);

const Color subtlColor = Color(0xFF444444);

Color incomeColor = Colors.green[600];
Color expenseColor = Colors.red[700];

const Color dangerColor = Color(0xFFC40234);
const Color disabledColor = subtlColor;

const Color noCategoryColor = Colors.grey;

const EdgeInsets contentPadding = EdgeInsets.only(top: contentSpacing, left: contentSpacing, right: contentSpacing);
const EdgeInsets contentPaddingDense = EdgeInsets.only(top: spacingUnit, left: contentSpacing, right: contentSpacing);
const EdgeInsets contentPaddingHorizontalOnly = EdgeInsets.symmetric(horizontal: contentSpacing);

const String feedbackAdress = 'cashflow@xf9.de';
const String feedbackAdressUrl = 'mailto:' + feedbackAdress;

const String currentVersion = 'v1.0.1';