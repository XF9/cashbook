import 'dart:math';

import 'package:cashflow/generated/i18n.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../helper/extensions.dart';

typedef LabelFunc = String Function(BuildContext context);

class EvaluationChartValue{
  final double value;
  final Color color;
  final LabelFunc labelFunc;

  EvaluationChartValue({@required this.value, @required this.color, @required this.labelFunc});
}

class EvaluationChartEntry{
  final List<EvaluationChartValue> values;
  final LabelFunc labelFunc;

  EvaluationChartEntry({@required this.values, @required  this.labelFunc});
}

class EvaluationChartData{
  final EvaluationChartEntry entry;
  final TextPainter labelText;
  final double y;
  final double height;

  EvaluationChartData({@required this.entry, @required this.y, @required this.height, @required this.labelText});
}

class ChartPainter extends CustomPainter {
  final BuildContext context;
  final List<EvaluationChartEntry> entries;
  TextStyle labelStyle;

  static const double barHeight = 20;
  static const double rowSpacing = 10;
  static const double labelSpacing = spacingUnit;

  final double scaleChart;
  final Function(double height) afterDraw;

  ChartPainter({@required this.context, @required this.entries, @required this.scaleChart, @required this.afterDraw}) {
    labelStyle = Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.white);
  }

  @override
  void paint(Canvas canvas, Size size) {
    if(entries.isNotEmpty)
      renderChart(canvas, size);    
  }

  void renderNoData(Canvas canvas, Size size){
    final labelText = TextPainter(
        text: TextSpan(
          text: I18n.of(context).noData,
          style: labelStyle,
        ),
        maxLines: 10,
        textDirection: TextDirection.rtl
      );

    labelText.layout();
    labelText.paint(canvas, Offset(size.width / 2 - labelText.width / 2, size.height / 2 - labelText.height / 2));
    afterDraw(size.height);
  }

  void renderChart(Canvas canvas, Size size){
    final linePaint = Paint()
      ..color = labelStyle.color
      ..strokeWidth = 1;

    double minValue = 0;
    double maxValue = 0;
    double maxLabelWidth = 0;
    double currentY = 0;

    final data = entries.map((entry){
      minValue = entry.values.fold(minValue, (previousValue, element) => min(previousValue, element.value));
      maxValue = entry.values.fold(maxValue, (previousValue, element) => max(previousValue, element.value));

      final labelText = TextPainter(
        text: TextSpan(
          text: entry.labelFunc(context),
          style: labelStyle,
        ),
        maxLines: 10,
        textDirection: TextDirection.rtl
      );
      labelText.layout(maxWidth: size.width / 3);

      final y = currentY;
      final height = max(labelText.height, barHeight) + rowSpacing;

      currentY += height;
      maxLabelWidth = max(maxLabelWidth, labelText.width);

      return EvaluationChartData(
        entry: entry,
        y: y,
        height: height,
        labelText: labelText
      );
    }).toList();

    final chartMaxXCeiled = (maxValue / 10).ceil() * 10;
    final chartMinXFloored = (minValue / 10).floor() * 10;
    final step = max(-chartMinXFloored, chartMaxXCeiled);

    final chartMaxX = chartMaxXCeiled == 0 ? 0 : step;
    final chartMinX = chartMinXFloored == 0 ? 0 : -step;

    final chartStart = maxLabelWidth + labelSpacing;
    final chartWidth = size.width - chartStart;
    final valueScale = chartWidth / (chartMaxX - chartMinX);
    final zero = ((-chartMinX) * valueScale) + chartStart;

    // Chart rows
    for(final entry in data){
      // Label
      entry.labelText.paint(canvas, Offset(maxLabelWidth - entry.labelText.width, entry.y + (entry.height - entry.labelText.height) / 2));
      
      for(final value in entry.entry.values)
      {
        // Bar
        final double radiusLeft = value.value.isNegative ? 5 : 0;
        final double radiusRight = value.value.isNegative ? 0 : 5;
        final barWidth = value.value * valueScale * scaleChart;
        canvas.drawRRect(
          RRect.fromRectAndCorners(
            Offset(zero, entry.y + (entry.height - barHeight) / 2) & Size(barWidth, barHeight),
            topLeft: Radius.circular(radiusLeft),
            bottomLeft: Radius.circular(radiusLeft),
            topRight: Radius.circular(radiusRight),
            bottomRight: Radius.circular(radiusRight)
          ),
          Paint()
            ..color = value.color
        );

        // Value
        final labelText = TextPainter(
          text: TextSpan(
            text: value.labelFunc(context),
            style: labelStyle,
          ),
          textDirection: TextDirection.rtl
        );
        labelText.layout();

        final labelFitsInBar = barWidth.abs() > labelText.width + 2 * labelSpacing;
        final labelFitsInSpace = value.value.isNegative
        ? zero - barWidth.abs() - labelSpacing - labelText.width > chartStart
        : zero + barWidth.abs() + labelSpacing + labelText.width < size.width;
        final labelBaseLine = entry.y + (entry.height - labelText.height) / 2;

        if(value.value.isNegative){
          if(labelFitsInBar)
            labelText.paint(canvas, Offset(zero + barWidth + labelSpacing, labelBaseLine));
          else if(!labelFitsInSpace)
            labelText.paint(canvas, Offset(zero - labelSpacing - labelText.width, labelBaseLine));
          else
            labelText.paint(canvas, Offset(zero + barWidth - labelSpacing - labelText.width, labelBaseLine));
        }else{
          if(labelFitsInBar)
            labelText.paint(canvas, Offset(zero + barWidth - labelSpacing - labelText.width, labelBaseLine));
          else if(!labelFitsInSpace)
            labelText.paint(canvas, Offset(zero + labelSpacing, labelBaseLine));
          else
            labelText.paint(canvas, Offset(zero + barWidth + labelSpacing, labelBaseLine));
        }
      }
    }

    // Lines
    var line = chartMinX;
    final barAreaHeight = data.last.y + data.last.height;
    var chartheight = 0.0;

    while(line <= chartMaxX){
      canvas.drawLine(Offset(zero + line * valueScale ,0), Offset(zero + line * valueScale, barAreaHeight), linePaint);
      final labelText = TextPainter(
        text: TextSpan(
          text: line.asCurrency(context),
          style: labelStyle,
        ),
        textDirection: TextDirection.rtl
      );
      labelText.layout();
      if (line == chartMaxX)
        labelText.paint(canvas, Offset((zero + line * valueScale) - labelText.width, barAreaHeight + labelSpacing));
      else
        labelText.paint(canvas, Offset((zero + line * valueScale) - labelText.width / 2, barAreaHeight + labelSpacing));
      line += step;
      chartheight = barAreaHeight + labelSpacing + labelText.height;
    }

    afterDraw(chartheight);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}