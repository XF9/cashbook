import 'package:flutter/material.dart';

import '../../generated/i18n.dart';
import '../../helper/navigationHelper.dart';
import '../base.dart';
import '../constants.dart';
import '../shared/openEndDrawerButton.dart';
import 'chart.dart';
import 'filter.dart';

class Evaluation extends StatefulWidget {
  const Evaluation({Key key}) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<Evaluation> {

  ChartFilter filter = ChartFilter(
    from: DateTime(DateTime.now().year, DateTime.now().month, 1),
    to: DateTime.now(),
    mainAxis: GroupBy.store,
    value: Value.total,
    sortBy: SortBy.value
  );

  void _updateFilter(ChartFilter newFilter){
    setState(() {
      filter = newFilter; 
    });
  }

  @override
  Widget build(BuildContext context) {

    return BaseLayout(
      title: I18n.of(context).pagesEvaluation,
      currentRoute: Routes.Evaluation,
      endDrawer: Filter(_updateFilter, filter),
      appBarActions: <Widget>[
        OpenEndDrawerButton(),
      ],
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(contentSpacing),
            child: Chart(filter: filter)
          )
        ],
      )
    );
  }
}