import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../generated/i18n.dart';
import '../shared/endDrawerFilter.dart';
import '../shared/filterHeader.dart';
import '../shared/groups/datepickerGroup.dart';
import '../shared/groups/selectGroup.dart';

enum GroupBy{
  date,
  store,
  category
}

enum Value{
  total,
  incomeExpense,
  count
}

enum SortBy{
  key,
  value
}

class ChartFilter{
  GroupBy mainAxis;
  Value value;
  SortBy sortBy;
  DateTime from;
  DateTime to;

  ChartFilter({this.mainAxis, this.value, this.sortBy, this.from, this.to});
}

class Filter extends StatefulWidget{

  final void Function(ChartFilter) onChange;
  final ChartFilter filter;
  
  const Filter(this.onChange, this.filter, {Key key}) : super(key: key);

  @override
  _State createState() => _State(filter);
}

class _State extends State<Filter> {

  GroupBy mainAxis;
  Value value;
  SortBy sortBy;
  DateTime from;
  DateTime to;

  _State(ChartFilter filter){
    mainAxis = filter.mainAxis;
    value = filter.value;
    sortBy = filter.sortBy;
    from = filter.from;
    to = filter.to;
  }

  @override
  void setState(void Function() func) {
    super.setState(func);
    _notifyChanged();
  }

  void _notifyChanged(){
    widget.onChange(ChartFilter(
      mainAxis: mainAxis,
      value: value,
      sortBy: sortBy,
      from: from,
      to: to
    ));
  }

  @override
  Widget build(BuildContext context) {

    final filterOptions = <Widget>[

      FilterHeader(),

      // Groups
      SelectGroup<GroupBy>(
        I18n.of(context).evaluationFilterGroupByTitle,
        mainAxis,
        [
          SelectGroupItem(
            value: GroupBy.date,
            label: I18n.of(context).evaluationFilterGroupByOptionMonth
          ),
          SelectGroupItem(
            value: GroupBy.store,
            label: I18n.of(context).evaluationFilterGroupByOptionStore
          ),
          SelectGroupItem(
            value: GroupBy.category,
            label: I18n.of(context).evaluationFilterGroupByOptionCategory
          ),
        ],
        
        (newValue) => setState(() {
          mainAxis = newValue;
        }),
      ),

      // Value
      SelectGroup<Value>(
        I18n.of(context).evaluationFilterValueTitle,
        value,
        [
          SelectGroupItem(
            value: Value.total,
            label: I18n.of(context).evaluationFilterValueOptionTotal
          ),
          SelectGroupItem(
            value: Value.incomeExpense,
            label: I18n.of(context).evaluationFilterValueOptionSplit
          ),
          SelectGroupItem(
            value: Value.count,
            label: I18n.of(context).evaluationFilterValueOptionCount
          ),
        ],
        (newValue) => setState(() {
          value = newValue;
        }),
      ),

      // Sorting
      SelectGroup<SortBy>(
        I18n.of(context).evaluationFilterSortByTitle,
        sortBy,
        [
          SelectGroupItem(
            value: SortBy.key,
            label: I18n.of(context).evaluationFilterSortByOptionMainAxis
          ),
          SelectGroupItem(
            value: SortBy.value,
            label: I18n.of(context).evaluationFilterSortByOptionValue
          ),
        ],
        (newValue) => setState(() {
          sortBy = newValue;
        }),
      ),

      DatepickerGroup(
        I18n.of(context).evaluationFilterRangeFromTitle, 
        from,
        (newValue) => setState(() {
          from = newValue;
        }),
        to: to
      ),

      DatepickerGroup(
        I18n.of(context).evaluationFilterRangeToTitle, 
        to,
        (newValue) => setState(() {
          to = newValue;
        }),
        from: from
      ),
    ];

    return EndDrawerFilter
    (
      children: filterOptions,
    );
  }
}