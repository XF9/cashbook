import 'package:flutter/material.dart';

import '../../database/database.dart';
import '../../generated/i18n.dart';
import '../../helper/extensions.dart';
import '../../helper/transactionHelper.dart';
import '../../provider/transactionProvider.dart';
import '../constants.dart';
import '../shared/noData.dart';
import 'chartpainter.dart';
import 'filter.dart';

class Chart extends StatefulWidget {
  final ChartFilter filter;

  const Chart({Key key, @required this.filter}) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<Chart> with SingleTickerProviderStateMixin {
  bool isPristine = true;
  List<Transaction> transactions = [];
  List<EvaluationChartEntry> data = [];
  bool hasTransactions = false;

  Animation<double> _animation;
  AnimationController controller;
  double scaleChart = 0;
  double chartHeight = 100;

  void _startAnimation(){
    if(controller == null){
      controller = AnimationController(
        duration: const Duration(milliseconds: 500),
        vsync: this
      );

      controller.forward();

      controller.addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.stop();
        } else if (status == AnimationStatus.dismissed) {
          controller.stop();
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    TransactionProvider.instance.startObserving(_fetchData);
    _fetchData();
    _startAnimation();
  }

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    _fetchData();
  }

  Future _fetchData() async {
    final transactionData = await TransactionProvider.instance
        .getTransactions(from: widget.filter.from, to: widget.filter.to);

    setState(() {
      transactions = transactionData;
      hasTransactions = transactionData.isNotEmpty;
      isPristine = false;
      data = [];
    });
  }

  void buildData(BuildContext context) {
    List<TransactionGroup> groups;
    switch (widget.filter.mainAxis) {
      case GroupBy.date:
        groups = transactions
            .groupBy<dynamic>((t) => DateTime(t.date.year, t.date.month, 1))
            .values
            .map((transactions) => TransactionGroup(
                (context) => transactions.first.date.asMonth(context),
                transactions))
            .toList();
        break;
      case GroupBy.store:
        groups = transactions
            .groupBy<dynamic>((t) => t.store.name)
            .values
            .map((transactions) => TransactionGroup(
                (context) => transactions.first.store.name, transactions))
            .toList();
        break;
      case GroupBy.category:
        groups = transactions
            .groupBy<dynamic>((t) => t.category?.name ?? '')
            .values
            .map((transactions) => TransactionGroup(
                (context) =>
                    transactions.first.category?.name ??
                    I18n.of(context).categoryNoCategoryName,
                transactions))
            .toList();
        break;
    }

    switch (widget.filter.sortBy) {
      case SortBy.key:
        switch (widget.filter.mainAxis) {
          case GroupBy.date:
            groups.sort((a, b) =>
                a.transactions.first.date.compareTo(b.transactions.first.date));
            break;
          case GroupBy.category:
          case GroupBy.store:
            groups.sort(
                (a, b) => a.labelFunc(context).compareTo(b.labelFunc(context)));
            break;
        }
        break;
      case SortBy.value:
        switch (widget.filter.value) {
          case Value.total:
          case Value.incomeExpense:
            groups.sort((a, b) => a.total.compareTo(b.total));
            break;
          case Value.count:
            groups.sort((a, b) =>
                a.transactions.length.compareTo(b.transactions.length));
            break;
        }
        break;
    }

    List<EvaluationChartEntry> chartData;
    switch (widget.filter.value) {
      case Value.total:
        chartData = groups
            .map((e) => EvaluationChartEntry(labelFunc: e.labelFunc, values: [
                  EvaluationChartValue(
                    color: e.total > 0 ? incomeColor : expenseColor,
                    value: e.total,
                    labelFunc: (context) => e.total.asCurrency(context),
                  )
                ]))
            .toList();
        break;
      case Value.incomeExpense:
        chartData = groups
            .map((e) => EvaluationChartEntry(labelFunc: e.labelFunc, values: [
                  EvaluationChartValue(
                    color: incomeColor,
                    value: e.income,
                    labelFunc: (context) => e.income > 0 ? e.income.asCurrency(context) : '',
                  ),
                  EvaluationChartValue(
                    color: expenseColor,
                    value: e.expense,
                    labelFunc: (context) => e.expense < 0 ? e.expense.asCurrency(context) : '',
                  )
                ]))
            .toList();
        break;
      case Value.count:
        chartData = groups
            .map((e) => EvaluationChartEntry(labelFunc: e.labelFunc, values: [
                  EvaluationChartValue(
                      color: subtlColor,
                      value: e.transactions.length.toDouble(),
                      labelFunc: (_) => e.transactions.length.toString())
                ]))
            .toList();
        break;
    }

    setState(() {
      data = chartData;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isPristine) 
      return const LinearProgressIndicator();

    if(!hasTransactions)
        return const NoData();

    if (data.isEmpty) 
      buildData(context);

    _animation = Tween(begin: 0.0, end: 1.0).animate(controller)
    ..addListener(() {
      setState(() {
        scaleChart = _animation.value;
      });
    });

    return Container(
      height: chartHeight,
      child: CustomPaint(
        painter: ChartPainter(entries: data, context: context, scaleChart: scaleChart, afterDraw: _updateContainerHeight),
      ),
    );
  }

  void _updateContainerHeight(double height){
    // We need an addPostFrameCallback, otherwise we would change the height during build
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() => chartHeight = height);
    });
  }
}
