import 'package:cashflow/helper/extensions.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:cashflow/ui/dashboard/currencyWithArrow.dart';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MonthDataHead extends StatelessWidget {

  final DateTime month;
  final double total;
  final double income;
  final double expense;

  const MonthDataHead({Key key, this.month, @required this.total, @required this.income, @required this.expense}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: contentSpacing),
          child: Text(
            month.asMonth(context),
            style: Theme.of(context).textTheme.headline6
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: contentSpacing),
          child: Text(
            total.asCurrency(context),
            style: Theme.of(context).textTheme.headline4.copyWith(color: total < 0 ? expenseColor : incomeColor)
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: contentSpacing * 1.5, bottom: contentSpacing),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              CurrencyValueWithArrow(value: income, isNegative: false,),
              CurrencyValueWithArrow(value: expense, isNegative: true,),
            ],
          ),
        ),
      ],
    );
  }  
}