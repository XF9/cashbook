import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../helper/extensions.dart';
import '../constants.dart';
import 'monthData.dart';

class TransactionTableHead extends StatelessWidget{

  final String title;
  final Color valueColor;
  final List<CategoryValues> data;

  const TransactionTableHead({Key key, @required this.title, @required this.valueColor, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: spacingUnit / 2),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: Theme.of(context).textTheme.headline6
          ),
          Text(
            data.fold<double>(.0, (previousValue, element) => previousValue + element.values.last.value).asCurrency(context),
            style: Theme.of(context).textTheme.headline6.copyWith(color: valueColor)
          )
        ],
      ),
    );
  }
}