import 'package:cashflow/helper/extensions.dart';
import 'package:cashflow/ui/constants.dart';


import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CurrencyValueWithArrow extends StatelessWidget{

  final double value;
  final bool isNegative;

  const CurrencyValueWithArrow({Key key, @required this.value, @required this.isNegative}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Row(
      children: [
        if (isNegative) 
          Icon(Icons.trending_down, color: expenseColor) 
        else 
          Icon(Icons.trending_up, color: incomeColor),
        Padding(
          padding: const EdgeInsets.only(left: spacingUnit),
          child: Text(
            value.asCurrency(context),
            style: Theme.of(context).textTheme.headline6.copyWith(color: isNegative ? expenseColor : incomeColor),
          ),
        )
      ],
    );
  }
}