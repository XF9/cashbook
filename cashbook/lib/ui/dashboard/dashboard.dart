import 'package:cashflow/ui/dashboard/backupReminder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../generated/i18n.dart';
import '../../helper/extensions.dart';
import '../../helper/navigationHelper.dart';
import '../base.dart';
import '../constants.dart';
import 'monthData.dart';

class DashBoard extends StatefulWidget {

  const DashBoard();

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<DashBoard> with SingleTickerProviderStateMixin{

  @override
  Widget build(BuildContext context) {
    return BaseLayout(
      title: I18n.of(context).pagesDashboard,
      currentRoute: Routes.DashBoard,
      child: ListView(
        children: <Widget>[
          AnimatedSize(
            vsync: this,
            duration: const Duration(milliseconds: 500),
            child: BackupReminder()
          ),
          Padding(
              padding: const EdgeInsets.only(
                bottom: 80
              ),
              child: MonthData(
                from: DateTime.now().firstDayInMonth(),
                to: DateTime.now())),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => NavigationHelper.navigateTo(context, Routes.AddTransaction),
        backgroundColor: primaryColor,
        foregroundColor: backgroundColor,
        child: const Icon(Icons.add),
      ),
    );
  }
}