import 'package:cashflow/database/database.dart';
import 'package:cashflow/generated/i18n.dart';
import 'package:cashflow/helper/extensions.dart';
import 'package:cashflow/provider/transactionProvider.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:cashflow/ui/dashboard/chart.dart';
import 'package:cashflow/ui/dashboard/monthDataHead.dart';
import 'package:cashflow/ui/dashboard/transactionTableGroup.dart';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MonthData extends StatefulWidget {
  final DateTime from;
  final DateTime to;

  const MonthData({@required this.from, @required this.to, Key key})
      : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<MonthData> {
  bool isPristine = true;
  List<CategoryValues> income = [];
  List<CategoryValues> expense = [];
  CategoryValues overall;

  @override
  void initState() {
    super.initState();
    TransactionProvider.instance.startObserving(_fetchData);
    _fetchData();
  }

  @override
  void dispose(){
    super.dispose();
    TransactionProvider.instance.endObserving(_fetchData);
  }

  Category _getDefaultCategory(BuildContext context) => Category(
    id: -1,
    name: I18n.of(context).categoryNoCategoryName,
    color: noCategoryColor);

  Future _fetchData() async {
    final transactionData =
        await TransactionProvider.instance.getTransactions(
          from: widget.from, 
          to: widget.to);
    final transactionsPerDay = transactionData.groupBy((t) => DateTime.utc(t.date.year, t.date.month, t.date.day));
    int currentX = 1;
    var currentDate = DateTime.utc(widget.from.year, widget.from.month, widget.from.day);
    final toDate = DateTime.utc(widget.to.year, widget.to.month, widget.to.day).add(const Duration(days: 1));

    final overallData =
        CategoryValues(Category(color: Colors.white, name: 'Overall'));
    overallData.values.add(ChartPoint(0, 0, 0));

    List<CategoryValues> incomeData = [];
    List<CategoryValues> expenseData = [];

    while (currentDate.isBefore(toDate)) {
      // All transactions for "today"
      final transactionsForCurrentDay =
          transactionsPerDay.keys.contains(currentDate)
              ? transactionsPerDay[currentDate]
              : <Transaction>[];

      // All categories of today with their values
      final categoryIncomeDataForCurrentDay = _groupByCategory(transactionsForCurrentDay
        .where((element) => element.value > 0)
        .toList());
      final categoryExpenseDataForCurrentDay = _groupByCategory(transactionsForCurrentDay
        .where((element) => element.value < 0)
        .toList());
      
      // Update categoryData
      incomeData = _extend(
          currentX,
          incomeData,
          categoryIncomeDataForCurrentDay);
      expenseData = _extend(
          currentX,
          expenseData,
          categoryExpenseDataForCurrentDay);
      overallData.values.add(ChartPoint(
          currentX,
          [
            ...categoryIncomeDataForCurrentDay,
            ...categoryExpenseDataForCurrentDay
          ].fold<double>(0,
                  (previousValue, element) => previousValue + element.value) +
              overallData.values.last.value,
          0));
      // move to next day
      currentX++;
      currentDate = currentDate.add(const Duration(days: 1));
    }

    setState(() {
      isPristine = false;
      income = incomeData;
      expense = expenseData;
      overall = overallData;
    });
  }

  List<_CategoryValue> _groupByCategory(List<Transaction> transactions){
    final result = <_CategoryValue>[];
      transactions
          .groupBy((t) => t.category?.id ?? -1)
          .forEach((key, value) {
            final sum = value.fold<double>(
              .0,
              (previousValue, element) => previousValue += element.value
            );
            result.add(_CategoryValue(value.first.category ?? _getDefaultCategory(context), sum));
      });
      return result;
  }

  List<CategoryValues> _extend(int x, List<CategoryValues> previousData,
      List<_CategoryValue> currentData) {
    double currentY = .0;

    // Compute added categories
    final newCategories = currentData
        .where((currentElement) => !previousData.any((previousElement) =>
            currentElement.category.id == previousElement.category.id))
        .toList();
    final newData = <CategoryValues>[];

    for(final categoryValue in newCategories){
      final dataSet = CategoryValues(categoryValue.category);
      dataSet.values.add(ChartPoint(x - 1, 0, 0));
      dataSet.values.add(ChartPoint(x, categoryValue.value, currentY));
      newData.add(dataSet);
      currentY += categoryValue.value;
    }

    // Compute existing categories
    for(final previousElement in previousData)
    {
      final change = currentData
          .firstWhere(
              (currentElement) =>
                  previousElement.category.id == currentElement.category.id,
              orElse: () => _CategoryValue(previousElement.category, 0))
          .value;
      final newValue = previousElement.values.last.value + change;
      previousElement.values.add(ChartPoint(x, newValue, currentY));
      currentY += newValue;
    }

    // Combine categories
    return [...newData, ...previousData];
  }

  @override
  Widget build(BuildContext context) {
    if (isPristine) 
      return const LinearProgressIndicator();

    final overallValue = overall.values.last.value;
    final overallIncome = income.fold<double>(.0, (previousValue, element) => previousValue + element.values.last.value);
    final overallExpense = expense.fold<double>(.0, (previousValue, element) => previousValue + element.values.last.value);
    
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: spacingUnit),
          child: Padding(
            padding: const EdgeInsets.all(contentSpacing),
            child: Column(
              children: [
                MonthDataHead(
                  month: widget.from,
                  total: overallValue,
                  income: overallIncome,
                  expense: overallExpense,
                ),
                const Divider(),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: contentSpacing),
                  child: Chart(
                    expense: expense,
                    income: income,
                    overall: overall,
                    from: widget.from,
                    to: widget.to),
                ),
                const Divider(),
                Padding(
                  padding: const EdgeInsets.only(top: contentSpacing),
                  child: TransactionTableGroup(
                    title: I18n.of(context).transactionIncome,
                    data: income,
                    valueColor: incomeColor,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: contentSpacing),
                  child: TransactionTableGroup(
                    title: I18n.of(context).transactionExpense,
                    data: expense,
                    valueColor: expenseColor,
                  ),
                ),
              ],
            ),
          )
        ),
      ],
    );
  }
}

class _CategoryValue {
  final Category category;
  final double value;

  _CategoryValue(this.category, this.value);
}

class CategoryValues {
  final Category category;
  List<ChartPoint> values;

  CategoryValues(this.category) {
    values = [];
  }
}

class ChartPoint {
  final int x;
  final double value;
  final double offset;

  ChartPoint(this.x, this.value, this.offset);
}
