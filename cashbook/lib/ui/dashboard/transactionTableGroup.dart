import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'monthData.dart';
import 'transactionTableHead.dart';
import 'transactionTableItem.dart';

class TransactionTableGroup extends StatelessWidget{

  final String title;
  final List<CategoryValues> data;
  final Color valueColor;

  const TransactionTableGroup({Key key, @required this.title, @required this.data, @required this.valueColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return Column(
        children: <Widget>[
          TransactionTableHead(title: title, valueColor: valueColor, data: data),
          ...data.map((e) => TransactionTableItem(
              data: e,
              valueColor: valueColor,
            )
          )
        ],
      );
  }
}