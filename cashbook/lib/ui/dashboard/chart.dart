import 'package:cashflow/ui/dashboard/chartpainter.dart';
import 'package:cashflow/ui/dashboard/monthData.dart';
import 'package:flutter/material.dart';

class Chart extends StatefulWidget{
  const Chart({Key key, @required this.income, @required this.expense, @required this.overall, @required this.from, @required this.to}) : super(key: key);
  
  final List<CategoryValues> income;
  final List<CategoryValues> expense;
  final CategoryValues overall;

  final DateTime from;
  final DateTime to;

  @override
  _State createState() => _State();
}

class Spot{
  const Spot(this.date, {this.value}); 

  final DateTime date;
  final num value;
}

class _State extends State<Chart> with SingleTickerProviderStateMixin {

  Animation<double> _animation;
  AnimationController controller;
  double scaleChart = 0;

  void _startAnimation(){
    if(controller == null){
      controller = AnimationController(
        duration: const Duration(milliseconds: 1000),
        vsync: this
      );

      controller.forward();

      controller.addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.stop();
        } else if (status == AnimationStatus.dismissed) {
          controller.stop();
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _startAnimation();
  }

  @override
  void didUpdateWidget(Chart old){
    super.didUpdateWidget(old);
    _startAnimation();
  }

  @override
  Widget build(BuildContext context) {

    _animation = Tween(begin: 0.0, end: 1.0).animate(controller)
    ..addListener(() {
      setState(() {
        scaleChart = _animation.value;
      });
    });

    return AspectRatio(
      aspectRatio: 3,
      child: CustomPaint(
        painter: ChartPainter(context: context, income: widget.income, expense: widget.expense, overall: widget.overall, scaleChart: scaleChart, from: widget.from, to: widget.to),
      ),
    );
  }
}