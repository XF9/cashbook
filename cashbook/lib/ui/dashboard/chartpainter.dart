import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../helper/extensions.dart';
import '../constants.dart';
import 'monthData.dart';

class ChartPainter extends CustomPainter {
  final BuildContext context;

  final List<CategoryValues> income;
  final List<CategoryValues> expense;
  final CategoryValues overall;

  final DateTime from;
  final DateTime to;
  final double scaleChart;

  int maxX = 0;
  double maxY = 0;
  double minY = 0;
  int lineSpacing = 0;

  TextStyle labelStyle;
  double labelHeight;

  ChartPainter(
      {@required this.context,
      @required this.income,
      @required this.expense,
      @required this.overall,
      @required this.scaleChart,
      this.from,
      this.to}) {
    maxX = to.lastDayInMonth().day; // + Origin
    final lastIncomePoint = income.isNotEmpty ? income.last.values.last : null;
    final lastExpensePoint = expense.isNotEmpty ? expense.last.values.last : null;
    final lastIncome = lastIncomePoint != null
        ? lastIncomePoint.value + lastIncomePoint.offset
        : 0;
    final lastExpense = lastExpensePoint != null
        ? lastExpensePoint.value + lastExpensePoint.offset
        : 0;
    final largest = lastIncome > -lastExpense ? lastIncome : -lastExpense;
    lineSpacing = ((largest + (100 - largest % 100)) / 2).round();
    while (maxY <= lastIncome) 
      maxY += lineSpacing;
    while (minY >= lastExpense) 
      minY -= lineSpacing;
    // Label Settings
    labelStyle = Theme.of(context).textTheme.bodyText2;
    final tp = TextPainter(
        text:
            TextSpan(
              style: labelStyle, 
              text: 'Sample text for reference'),
        textAlign: TextAlign.right,
        textDirection: TextDirection.ltr);
    tp.layout();
    labelHeight = tp.height;
  }

  Offset _printLabels(Canvas canvas, double height, double yRange, int lineCount) {
    var leftSpacing = .0;
    final demolabel = TextPainter(
      text: TextSpan(
          style: labelStyle,
          text: 'Demotext'),
      textAlign: TextAlign.right,
      textDirection: TextDirection.ltr);
    demolabel.layout();
    final labelheight = demolabel.height;
    height -= labelHeight / 2;

    for (int i = 0; i < lineCount; i++) {
      final y = ((i * lineSpacing) / yRange * height) + labelheight / 2;
      final tp = TextPainter(
          text: TextSpan(
              style: labelStyle,
              text: (maxY - (i * lineSpacing)).asCurrency(context)),
          textAlign: TextAlign.right,
          textDirection: TextDirection.ltr);
      tp.layout();
      tp.paint(canvas, Offset(0, y - tp.height / 2));
      if (leftSpacing < tp.width) 
        leftSpacing = tp.width;
    }
    return Offset(leftSpacing + spacingUnit, labelheight / 2);
  }

  void _printYLines(Canvas canvas, double height, double width, double yRange,
      int lineCount, Offset spacing, Paint paint) {
    for (int i = 0; i < lineCount; i++) {
      final y = ((i * lineSpacing) / yRange * height) + spacing.dy;
      canvas.drawLine(Offset(spacing.dx, y), Offset(width, y), paint);
    }
  }

  void _printXTicks(Canvas canvas, double width, double chartHeight,
      Offset spacing, Paint paint, double tickHeight) {
    const daysBetween = 7;
    var currentDay = 1;
    while (currentDay < maxX) {
      final x = (currentDay / maxX * (width - spacing.dx)) + spacing.dx;
      canvas.drawLine(
          Offset(x, chartHeight + spacing.dy), Offset(x, chartHeight + spacing.dy + tickHeight), paint);
      final tp = TextPainter(
          text: TextSpan(
            style: labelStyle, 
            text: currentDay.toString()),
          textAlign: TextAlign.right,
          textDirection: TextDirection.ltr);
      tp.layout();
      tp.paint(canvas, Offset(x - tp.width / 2, chartHeight + tickHeight + spacing.dy));

      currentDay += daysBetween;
    }
  }

  void _printCartegoryData(
    Canvas canvas, 
    double width, 
    double chartHeight,
    Offset spacing, 
    double yRange) {
      for(final categoryValue in [...income, ...expense]){
        if(categoryValue.values.first.x > 0)
          categoryValue.values = [
            ChartPoint(0, 0, 0),
            ...categoryValue.values
          ];

        final points = _limitByScale(categoryValue.values)
            .map((value) =>
                _toOffset(value, width, chartHeight, yRange, spacing, true))
            .toList();
        final reversedOffsets = _limitByScale(categoryValue.values)
            .map((value) =>
                _toOffset(value, width, chartHeight, yRange, spacing, false))
            .toList()
            .reversed
            .toList();
            
        final fill = Path();
        _drawPointsOnPath(fill, points);
        _drawPointsOnPath(fill, reversedOffsets, reverse: true);
        canvas.drawPath(
          fill,
          Paint()
          ..style = PaintingStyle.fill
            ..color = categoryValue.category.color.withAlpha(30)
        );

        final line = Path();
        _drawPointsOnPath(line, points);
        canvas.drawPath(
          line,
          Paint()
          ..style = PaintingStyle.stroke
            ..color = categoryValue.category.color.withAlpha(60)
            ..strokeWidth = 1
        );
      }
  }

  void _printOverall(Canvas canvas, double width, double chartHeight, Offset spacing, double yRange) {
    final points = _limitByScale(overall.values)
        .map((value) => _toOffset(value, width, chartHeight, yRange, spacing, true))
        .toList();

    final line = Path();
    _drawPointsOnPath(line, points);
    canvas.drawPath(
      line,
      Paint()
      ..style = PaintingStyle.stroke
        ..color = overall.category.color
        ..strokeWidth = 1.5
    );
  }

  void _drawPointsOnPath(Path path, List<Offset> points, {bool reverse = false}){
    for(int index = 0; index < points.length; index++){
      if(index == 0)
      {
        if(reverse)
          path.lineTo(points[index].dx, points[index].dy);
        else
          path.moveTo(points[index].dx, points[index].dy);
      }else{
        final prev = points[index - 1];
        final cur = points[index];
        path.cubicTo(
          cur.dx - (cur.dx - prev.dx) * .5, 
          prev.dy, 
          prev.dx + (cur.dx - prev.dx) * .5, 
          cur.dy,  
          cur.dx, 
          cur.dy);
      }
    }
  }
 
  List<ChartPoint> _limitByScale(List<ChartPoint> source){
    final max = maxX * scaleChart;
    return source.where((element) => element.x < max).toList();
  }

  Offset _toOffset(ChartPoint point, double width, double height, double yRange,
      Offset spacing, bool drawValue) {
    var pointY = drawValue ? point.offset + point.value : point.offset;

    final maxAbsY = maxY > -minY ? maxY : -minY;
    if (pointY.isNegative && pointY < -maxAbsY) 
      pointY = -maxAbsY;
    if (!pointY.isNegative && pointY > maxAbsY) 
      pointY = maxAbsY;

    final x = (point.x / maxX * (width - spacing.dx)) + spacing.dx;
    final y = ((maxY - pointY) / yRange * height) + spacing.dy;
    return Offset(x, y);
  }

  @override
  void paint(Canvas canvas, Size size) {
    final yRange = maxY - minY;
    final yLineCount = (maxY / lineSpacing + (-minY) / lineSpacing + 1).round();
    const tickHeight = 3.0;
    final bottomSpacing = labelHeight + tickHeight;
    var chartHeight = size.height - bottomSpacing;

    final linePaint = Paint()
      ..color = labelStyle.color
      ..strokeWidth = .5;

    final chartOffest = _printLabels(canvas, chartHeight, yRange, yLineCount);
    chartHeight -= chartOffest.dy;
    _printYLines(canvas, chartHeight, size.width, yRange, yLineCount, chartOffest, linePaint);
    _printXTicks(canvas, size.width, chartHeight, chartOffest, linePaint, tickHeight);
    _printCartegoryData(canvas, size.width, chartHeight, chartOffest, yRange);
    _printOverall(canvas, size.width, chartHeight, chartOffest, yRange);
  }

  @override
  bool shouldRepaint(ChartPainter oldDelegate) {
    return oldDelegate.scaleChart != scaleChart;
  }
}
