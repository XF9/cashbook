import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../helper/extensions.dart';
import '../constants.dart';
import '../shared/categoryIndicator.dart';
import 'monthData.dart';

class TransactionTableItem extends StatelessWidget{

  final CategoryValues data;
  final Color valueColor;

  const TransactionTableItem({Key key, @required this.data, @required this.valueColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: spacingUnit / 4),
      child: IntrinsicHeight(
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: spacingUnit / 2, right: spacingUnit),
              child: CategoryIndicator(category: data.category),
            ),
            Expanded(
              child: Text(
                data.category.name,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
            Text(
              data.values.last.value.asCurrency(context),
              style: Theme.of(context).textTheme.bodyText1.copyWith(color: valueColor)
            )          
          ],
        ),
      ),
    );
  }  
}