import 'package:cashflow/database/fileBase.dart';
import 'package:cashflow/generated/i18n.dart';
import 'package:cashflow/helper/navigationHelper.dart';
import 'package:cashflow/ui/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BackupReminder extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<BackupReminder> {

  var display = false;
  
  @override
  void initState(){
    super.initState();
    FileBase().startObserving(_loadData);
    _loadData();
  }

  @override
  void dispose(){
    super.dispose();
    FileBase().endObserving(_loadData);
  }

  Future _loadData() async {
    final data = await FileBase().load();
    setState(() {
      display = data.nextBackupReminder.isBefore(DateTime.now());
    });
  }

  void _dismiss(){
    FileBase().saveNextBackupReminder(DateTime.now().add(const Duration(days: 7)));
  }

  @override
  Widget build(BuildContext context) {

    if(!display)
      //Use if you want to test the backup rmeinder
      // return RaisedButton(
      //   onPressed: () async{
      //     await FileBase().saveNextBackupReminder(DateTime.now().subtract(const Duration(days: 1)));
      //     _loadData();
      //   },
      // );
      return Container();
    
    return Padding(
      padding: contentPadding,
      child: Card(
        elevation: cardElevation,
        child: Column(
          children: [
            Padding(
              padding: contentPadding,
              child: Text(
                I18n.of(context).dashboardBackupReminderText,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            ButtonBar(
              children: [
                TextButton(
                  child: Text(I18n.of(context).dashboardBackupReminderCreate),
                  onPressed: () => NavigationHelper.navigateTo(context, Routes.SettingsBackup)
                ),
                TextButton(
                  child: Text(I18n.of(context).dashboardBackupReminderDismiss),
                  onPressed: _dismiss
                )
              ],
            )
          ],
        ),
      ),
    );
  }  
}