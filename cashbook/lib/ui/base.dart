import 'package:cashflow/ui/drawer/mainDrawer.dart';
import 'package:cashflow/ui/constants.dart';

import 'package:flutter/material.dart';

class ErrorData{
  final dynamic error;
  final StackTrace trace;

  ErrorData({@required this.error, this.trace});
}

class BaseLayout extends StatelessWidget{
  final String title;
  final Widget child;
  final bool automaticallyImplyLeading;
  final bool hideHeader;
  final List<Widget> appBarActions;
  final PreferredSizeWidget tabBar;
  final Widget floatingActionButton;
  final String currentRoute;
  final Widget endDrawer;
  final ErrorData errorData;

  const BaseLayout({this.title, this.child, this.automaticallyImplyLeading, this.appBarActions, this.tabBar, this.floatingActionButton, this.currentRoute, this.endDrawer, this.hideHeader, this.errorData});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: hideHeader ?? false
        ? null
        : AppBar(
        title: Text(
          title ?? 'CashFlow Meter',
          style: Theme.of(context).textTheme.headline6,
        ),
        automaticallyImplyLeading: automaticallyImplyLeading ?? true,
        actions: appBarActions,
        bottom: tabBar,
      ),
      body: Stack(
        children: [
          Container(
            height: 500,  
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  backgroundColor,
                  backgroundColorTo
                ],
                begin: Alignment.center,
                end: Alignment.topCenter
              )
            ),
          ),
          child,
        ],
      ),
      drawer: MainDrawer(currentRoute: currentRoute),
      endDrawer: endDrawer,
      floatingActionButton: floatingActionButton,
    );
  }
}