import 'dart:io';

import 'package:cashflow/helper/navigationHelper.dart';
import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'generated/i18n.dart';
import 'ui/constants.dart';

void main() {

  final localizationOptionsDe = LocalizationOptions('de',
    dialogReportModeDescription: 'Das hätte nicht passieren sollen. Kannst du mir einen Fehlerbericht schicken damit ich diesen Fehler beheben kann?',
    dialogReportModeAccept: 'Senden',
    dialogReportModeCancel: 'Nicht senden',
    dialogReportModeTitle: 'Fehler'
  );

  final localizationOptionsEn = LocalizationOptions('en',
    dialogReportModeDescription: 'This shouldn\'t have happened. Would you mind sending me a bug report so I can fix this issue?',
    dialogReportModeTitle: 'Error'
  );

  // In Debug, just log to console
  final debugOptions =
    CatcherOptions(
      DialogReportMode(), 
      [
        ConsoleHandler()
      ], 
      localizationOptions: [
        localizationOptionsDe,
        localizationOptionsEn
      ]
    );
      
  // In release, send Email
  final releaseOptions = 
    CatcherOptions(
      DialogReportMode(),
      [
        EmailManualHandler(
          [feedbackAdress], 
          emailTitle: 'CashFlow Meter crashed :(',
          emailHeader: 'Locale: ${Platform.localeName}'
        )
      ],
      localizationOptions: [
        localizationOptionsDe,
        localizationOptionsEn
      ]
    );

  Catcher(rootWidget: MyApp(), debugConfig: debugOptions, releaseConfig: releaseOptions, ensureInitialized: true);
}

class MyApp extends StatelessWidget {
  static const Brightness brightness = Brightness.dark;

  @override
  Widget build(BuildContext context) {
    const i18n = I18n.delegate;
    return MaterialApp(
      title: 'CashFlow',
      localizationsDelegates: const [
        i18n,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: i18n.supportedLocales,
      localeResolutionCallback:
          i18n.resolution(fallback: const Locale('en', 'US')),
      theme: ThemeData(
          brightness: brightness,
          accentColor: primaryColor,
          highlightColor: primaryColor,
          splashColor: primaryColor,
          toggleableActiveColor: primaryColor,
          scaffoldBackgroundColor: backgroundColor,
          canvasColor: backgroundColor,
          textTheme: TextTheme(
              headline6: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
              headline5: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              headline4: const TextStyle(color: Colors.white),
              subtitle1: const TextStyle(color: Colors.white, fontSize: 16),
              subtitle2: const TextStyle(
                color: Colors.grey,
                fontSize: 14,
              ),
              bodyText1: Theme.of(context)
                  .textTheme
                  .bodyText2
                  .copyWith(color: Colors.white, fontSize: 14),
              bodyText2: Theme.of(context)
                  .textTheme
                  .bodyText2
                  .copyWith(color: onBackgroundColor2, fontSize: 13)),
          buttonTheme: Theme.of(context).buttonTheme.copyWith(
                buttonColor: subtlColor,
                textTheme: ButtonTextTheme.primary,
                colorScheme: Theme.of(context)
                    .buttonTheme
                    .colorScheme
                    .copyWith(primary: primaryColor),
              ),
          primaryTextTheme:
              const TextTheme(headline6: TextStyle(color: primaryColor)),
          colorScheme: Theme.of(context).colorScheme.copyWith(
              primary: primaryColor,
              secondary: Colors.grey[300],
              onPrimary: Colors.black,
              surface: subtlColor,
              onSurface: Colors.white,
              brightness: brightness),
              cardColor: elevatedBackgroundColor,
          appBarTheme: Theme.of(context).appBarTheme.copyWith(
              textTheme: TextTheme(
                headline6: Theme.of(context).textTheme.headline6.copyWith(
                  color: Colors.white, 
                  fontWeight: FontWeight.normal
                )
              ),
              color: backgroundColor
          ),
          cardTheme: Theme.of(context).cardTheme.copyWith(color: elevatedBackgroundColor),
          dialogTheme: Theme.of(context).dialogTheme.copyWith(
              backgroundColor: backgroundColor,
              contentTextStyle: const TextStyle(color: Colors.white)
          )
      ),
      initialRoute: Routes.SplashScreen,
      routes: NavigationHelper.routes(),
      onGenerateRoute: NavigationHelper.onGenerateRoute,
      navigatorKey: Catcher.navigatorKey,
    );
  }
}
