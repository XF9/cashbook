import 'package:cashflow/database/database.dart';
import 'package:cashflow/helper/notifyChanged.dart';
import 'package:cashflow/provider/categoryProvider.dart';
import 'package:cashflow/provider/storeProvider.dart';

import 'package:flutter/widgets.dart';

class TransactionProvider extends NotifyChanged {
  factory TransactionProvider() => _instance;
  const TransactionProvider._();
  static const TransactionProvider _instance = TransactionProvider._();
  static TransactionProvider get instance => _instance;

  Future<List<Transaction>> getTransactions(
      {DateTime from, DateTime to}) async {
    final database = await Database().instance;
    final fromTimestamp = from?.millisecondsSinceEpoch ?? 0;
    final toTimestamp = to?.millisecondsSinceEpoch ??
        DateTime(DateTime.now().year + 1).millisecondsSinceEpoch;

    final List<Map<String, dynamic>> maps = await database.rawQuery(
        'SELECT '
        '[Transaction].id, '
        '[Transaction].value, '
        '[Transaction].date, '
        '[Transaction].note, '
        '[Store].id AS storeId, '
        '[Store].name AS storeName, '
        '[Category].id AS categoryId, '
        '[Category].color AS categoryColor, '
        '[Category].name AS categoryName '
        'FROM [Transaction] '
        'JOIN [Store] ON [Transaction].storeId = [Store].id '
        'LEFT JOIN [Category] ON [Transaction].categoryId = [Category].id '
        'where [Transaction].date >= ? and [Transaction].date < ?',
        <dynamic>[fromTimestamp, toTimestamp]);
    final List<Transaction> transactions = List.generate(maps.length, (i) {
      final data = maps[i];
      return Transaction(
          id: data['id'] as int,
          date: DateTime.fromMillisecondsSinceEpoch(data['date'] as int),
          value: data['value'] as double,
          note: data['note'] as String,
          store: Store(
              id: data['storeId'] as int, name: data['storeName'] as String),
          category: data['categoryId'] == null
              ? null
              : Category(
                  id: data['categoryId'] as int,
                  name: data['categoryName'] as String,
                  color: Color(data['categoryColor'] as int)));
    });

    return transactions;
  }

  Future saveTransaction(
      {@required DateTime date,
      @required double value,
      @required String storename,
      int id,
      String categoryName,
      bool setCategoryAsDefault,
      String note}) async {
    final database = await Database().instance;
    final storeList = await database
        .query('Store', where: 'name = ?', whereArgs: <dynamic>[storename]);
    final storeId = storeList.isNotEmpty
        ? storeList.first['id'] as int
        : await database.insert('Store', <String, dynamic>{'name': storename});
    final categoryId = categoryName != null && categoryName != ''
        ? await CategoryProvider.instance
            .getCategoryId(name: categoryName, createIfMissing: true)
        : null;

    final transaction = {
      'id': id,
      'value': value,
      'date': date.millisecondsSinceEpoch,
      'storeId': storeId,
      'categoryId': categoryId,
      'note': note
    };

    if (id == null)
      await database.insert('Transaction', transaction);
    else
      await database.update('Transaction', transaction,
          where: 'id = ?', whereArgs: <dynamic>[id]);

    if (setCategoryAsDefault && categoryId != null) {
      final store = await StoreProvider.instance.getStore(storeId);
      StoreProvider.instance.saveStore(store.id, store.name, categoryName);
    }

    notifyChanged();
  }

  Future deleteTransaction(int id) async {
    final database = await Database().instance;
    await database
        .delete('Transaction', where: 'id=?', whereArgs: <dynamic>[id]);
    notifyChanged();
  }
}
