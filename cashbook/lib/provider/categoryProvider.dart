import 'package:cashflow/database/database.dart';
import 'package:cashflow/helper/notifyChanged.dart';
import 'package:cashflow/provider/storeProvider.dart';
import 'package:cashflow/provider/transactionProvider.dart';
import 'package:flutter/widgets.dart';

class CategoryProvider extends NotifyChanged {
  factory CategoryProvider() => _instance;
  const CategoryProvider._();
  static const CategoryProvider _instance = CategoryProvider._();
  static CategoryProvider get instance => _instance;

  Future<int> saveCategory(
      {int id, @required String name, @required Color color}) async {
    final database = await Database().instance;
    final category = {'id': id, 'name': name, 'color': color.value};

    if (id == null)
      id = await database.insert('Category', category);
    else
      database.update('Category', category, where: 'id = ?', whereArgs: <dynamic>[id]);

    notifyChanged();
    return id;
  }

  Future<List<Category>> getCategories() async {
    final database = await Database().instance;
    final dataSet = await database.query('Category');
    return dataSet
        .map((data) => Category(
            id: data['id'] as int,
            name: data['name'] as String, 
            color: Color(data['color'] as int)))
        .toList();
  }

  Future<int> getCategoryId({@required String name, bool createIfMissing = false}) async {
    final dataBase = await Database().instance;
    final categories = await dataBase.query('Category', where: 'name = ?', whereArgs: <dynamic>[name]);
    final category = categories.isNotEmpty ? categories.first : null;

    if (category == null)
      return createIfMissing
          ? saveCategory(name: name, color: const Color(0xFF666666))
          : null;
    return category['id'] as int;
  }

  Future<void> mergeCategory({@required Category toBeDeleted, @required Category toBeMergedInto}) async {
    final database = await Database().instance;
    await database.transaction((txn) async {
      database.rawUpdate('UPDATE [Transaction] SET categoryId = ? WHERE categoryId = ?', <int>[toBeMergedInto.id, toBeDeleted.id]);
      database.rawUpdate('UPDATE [Store] SET categoryId = ? WHERE categoryId = ?', <int>[toBeMergedInto.id, toBeDeleted.id]);
      database.delete('Category', where: 'id = ?', whereArgs: <int>[toBeDeleted.id]);
    });
    TransactionProvider.instance.notifyChanged();
    StoreProvider.instance.notifyChanged();
    notifyChanged();
  }
}
