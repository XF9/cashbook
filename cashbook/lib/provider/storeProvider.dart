import 'package:cashflow/database/database.dart';
import 'package:cashflow/helper/notifyChanged.dart';
import 'package:cashflow/provider/categoryProvider.dart';
import 'package:cashflow/provider/transactionProvider.dart';
import 'package:flutter/widgets.dart';

class StoreProvider extends NotifyChanged {
  factory StoreProvider() => _instance;
  const StoreProvider._();
  static const StoreProvider _instance = StoreProvider._();
  static StoreProvider get instance => _instance;

  static const queryStores = 'SELECT '
      'Store.id as storeId, '
      'Store.name as storeName, '
      'Category.id as categoryId, '
      'Category.name as categoryName, '
      'Category.color as categoryColor '
      'FROM Store LEFT JOIN Category ON Store.categoryId = Category.id';
  static const queryStoresById = queryStores + ' WHERE Store.id = ?';

  Future<List<Store>> getStores() async {
    final database = await Database().instance;
    final dataSet = await database.rawQuery(queryStores);
    return dataSet.map((data) => _map(data)).toList();
  }

  Future<Store> getStore(int id) async {
    final database = await Database().instance;
    final dataSet = await database.rawQuery(queryStoresById, <dynamic>[id]);
    return _map(dataSet.first);
  }

  Future<void> saveStore(int id, String name, String defaultCategory) async {
    final database = await Database().instance;

    int defaultCategoryId;
    if (defaultCategory != '')
      defaultCategoryId = await CategoryProvider.instance
          .getCategoryId(name: defaultCategory, createIfMissing: true);

    final store = {'id': id, 'name': name, 'categoryId': defaultCategoryId};
    if (id != null)
      database
          .update('Store', store, where: 'id = ?', whereArgs: <dynamic>[id]);
    else
      database.insert('Store', store);

    notifyChanged();
  }

  Future<void> mergeStore({@required Store toBeDeleted, @required Store toBeMergedInto}) async{
    final database = await Database().instance;
    await database.transaction((txn) async {
      database.rawUpdate('UPDATE [Transaction] SET storeId = ? WHERE storeId = ?', <int>[toBeMergedInto.id, toBeDeleted.id]);
      database.delete('Store', where: 'id = ?', whereArgs: <int>[toBeDeleted.id]);
    });
    TransactionProvider.instance.notifyChanged();
    notifyChanged();
  }

  Store _map(Map<String, dynamic> data) {
    return Store(
      id: data['storeId'] as int,
      name: data['storeName'] as String,
      defaultCategory: data['categoryId'] != null
        ? Category(
            id: data['categoryId'] as int,
            name: data['categoryName'] as String,
            color: Color(data['categoryColor'] as int))
        : null,
    );
  }
}
