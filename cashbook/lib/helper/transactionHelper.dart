import 'package:flutter/widgets.dart';

import '../database/database.dart';
import 'extensions.dart';

enum GroupTransactionsByDate{
  day,
  month,
  year
}

typedef LabelFuncType = String Function(BuildContext context);

class TransactionGroup{
  TransactionGroup(this.labelFunc, this.transactions){
    total = transactions.fold<double>(0, (p, c) => p + c.value);
    income = transactions.fold<double>(0, (p, c) => p + (c.value >= 0 ? c.value : 0));
    expense = transactions.fold<double>(0, (p, c) => p + (c.value <= 0 ? c.value : 0));
  }

  LabelFuncType labelFunc;
  List<Transaction> transactions;
  double total;
  double income;
  double expense;
}

class TransactionHelper{
  DateTime _getGroupKey(Transaction transaction, GroupTransactionsByDate groupBy){
    switch(groupBy){
      case GroupTransactionsByDate.day:
        return DateTime(transaction.date.year, transaction.date.month, transaction.date.day);
      case GroupTransactionsByDate.month:
        return DateTime(transaction.date.year, transaction.date.month, 1);
      case GroupTransactionsByDate.year:
        return DateTime(transaction.date.year, 1, 1);
    }

    return DateTime.now();
  }

  String _getGroupLabel(DateTime groupKey, GroupTransactionsByDate groupBy, BuildContext context){
    switch(groupBy){
      case GroupTransactionsByDate.day:
        return groupKey.asDay(context);
      case GroupTransactionsByDate.month:
        return groupKey.asMonth(context);
      case GroupTransactionsByDate.year:
        return groupKey.asYear(context);
    }

    return groupKey.asDay(context);
  }

  List<TransactionGroup> groupTransactionsByDate(List<Transaction> transactions, GroupTransactionsByDate groupBy){
    transactions.sort((a, b) => a.date == b.date 
      ? b.store.name.compareTo(a.store.name) 
      : a.date.compareTo(b.date));
    final transactionGroups = transactions
      .reversed
      .groupBy((transaction) => _getGroupKey(transaction, groupBy));      
    return transactionGroups.keys.map((groupKey) => 
      TransactionGroup((context) => 
        _getGroupLabel(groupKey, groupBy, context), transactionGroups[groupKey]))
        .toList();
  }

  List<TransactionGroup> groupTransactionsByStore(List<Transaction> transactions){
    transactions.sort((a, b) => a.store.name.compareTo(b.store.name));
    final transactionGroups = transactions
      .reversed
      .groupBy((transaction) => transaction.store.name);      
    final groups = transactionGroups.keys.map((groupKey) => 
      TransactionGroup((_) => 
        groupKey, transactionGroups[groupKey])).toList();
    return groups.reversed.toList();
  }
}