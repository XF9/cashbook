class NotifyChanged{
  
  static final List<Function()> _subscribers = <Function()>[];
  const NotifyChanged();

  void notifyChanged(){
    for(final onChange in _subscribers)
      onChange();
  }

  void startObserving(Function() onChange){
    if(!_subscribers.contains(onChange))
      _subscribers.add(onChange);
  }

  void endObserving(Function() onChange){
    if(_subscribers.contains(onChange))
      _subscribers.remove(onChange);
  }
}