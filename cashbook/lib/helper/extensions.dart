
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

// localeOf will sometimes throw an error when building, not sure why
// so we use use 'en' as fallback till we can get the current locale

extension DateExtension on DateTime{
  String asShortDate(BuildContext context){
    final dateFormat = DateFormat.yMd(Localizations.localeOf(context, nullOk: true)?.languageCode ?? 'en');
    return dateFormat.format(this);
  }
  
  String asDay(BuildContext context){
    final dateFormat = DateFormat('EEE, dd.MM.yyyy', Localizations.localeOf(context, nullOk: true)?.languageCode ?? 'en');
    return dateFormat.format(this);
  }

  String asDayWithMonth(BuildContext context){
    final dateFormat = DateFormat('dd. MMM', Localizations.localeOf(context, nullOk: true)?.languageCode ?? 'en');
    return dateFormat.format(this);
  }

  String asMonth(BuildContext context){
    final dateFormat = DateFormat('MMMM yyyy', Localizations.localeOf(context, nullOk: true)?.languageCode ?? 'en');
    return dateFormat.format(this);
  }

  String asYear(BuildContext context){
    final dateFormat = DateFormat('yyyy', Localizations.localeOf(context, nullOk: true)?.languageCode ?? 'en');
    return dateFormat.format(this);
  }

  DateTime firstDayInMonth(){
    return DateTime(year, month, 1);
  }

  DateTime lastDayInMonth(){
    if(month == 12)
      return DateTime(year + 1, 1, 0);
      return DateTime(year, month + 1, 0);
  }

  DateTime date(){
    return DateTime(year, month, day);
  }
}

extension StringExtension on String{
  DateTime fromShortDate(BuildContext context){
    final dateFormat = DateFormat.yMd(Localizations.localeOf(context, nullOk: true)?.languageCode ?? 'en');
    return dateFormat.parse(this);
  }

  bool isCurrency(BuildContext context){
    final localeformat = NumberFormat.currency(locale: Localizations.localeOf(context, nullOk: true)?.languageCode ?? 'en');
    try{
      localeformat.parse(this);
      return true;
    }
    catch(_){
      return false;
    }
  }

  double asCurrency(BuildContext context){
    final localeformat = NumberFormat.currency(locale: Localizations.localeOf(context, nullOk: true)?.languageCode ?? 'en');
    return localeformat.parse(this).toDouble();
  }
}

extension NumberExtension on num{
  String asCurrency(BuildContext context){
    if(this == null)
      return '';

    final localeformat = NumberFormat.simpleCurrency(locale: Localizations.localeOf(context, nullOk: true)?.languageCode ?? 'en');
    return NumberFormat.currency(
      locale: localeformat.locale, 
      symbol: localeformat.currencySymbol
    ).format(this);
  }
}

extension Iterables<E> on Iterable<E> {
  Map<K, List<E>> groupBy<K>(K Function(E) keyFunction) => fold(
      <K, List<E>>{},
      (Map<K, List<E>> map, E element) =>
          map..putIfAbsent(keyFunction(element), () => <E>[]).add(element));
}