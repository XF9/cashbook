import 'package:cashflow/database/database.dart';
import 'package:cashflow/ui/about/about.dart';
import 'package:cashflow/ui/dashboard/dashboard.dart';
import 'package:cashflow/ui/evaluation/evaluation.dart';
import 'package:cashflow/ui/manageTransaction/addTransaction.dart';
import 'package:cashflow/ui/manageTransaction/editTransaction.dart';
import 'package:cashflow/ui/settings/backup.dart';
import 'package:cashflow/ui/settings/categories/categoryList.dart';
import 'package:cashflow/ui/settings/categories/editCategory.dart';
import 'package:cashflow/ui/settings/categories/mergeCategory.dart';
import 'package:cashflow/ui/settings/stores/editStore.dart';
import 'package:cashflow/ui/settings/stores/mergeStore.dart';
import 'package:cashflow/ui/settings/stores/storelist.dart';
import 'package:cashflow/ui/splashscreen/splashscreen.dart';
import 'package:cashflow/ui/transactionlist/transactionlist.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Routes{
  static const String SplashScreen = 'SplashScreen';

  static const String DashBoard = 'DashBoard';

  static const String TransactionList = 'Transaction/List';
  static const String AddTransaction = 'Transaction/Add';
  static const String EditTransaction = 'Transaction/Edit';

  static const String Evaluation = 'Evaluation';
    
  static const String SettingsStores = 'Settings/Stores';
  static const String SettingsStoresAdd = 'Settings/Stores/Add';
  static const String SettingsStoresEdit = 'Settings/Stores/Edit';
  static const String SettingsStoresMerge = 'Settings/Stores/Merge';

  static const String SettingsCategories = 'Settings/Categories';
  static const String SettingsCategoriesAdd = 'Settings/Categories/Add';
  static const String SettingsCategoriesEdit = 'Settings/Categories/Edit';
  static const String SettingsCategoriesMerge = 'Settings/Categories/Merge';

  static const String SettingsBackup = 'Settings/Backup';

  static const String About = 'About';
}

class NavigationHelper {
  static Map<String, Widget Function(BuildContext)> routes(){
    return {
      Routes.SplashScreen: (_) => const SplashScreen(),

      Routes.DashBoard: (_) => const DashBoard(),

      Routes.TransactionList: (_) => const TransactionList(),
      Routes.AddTransaction: (_) => const AddTransaction(),
      
      Routes.Evaluation: (_) => const Evaluation(),

      Routes.SettingsStores: (_) => const StoreList(),      
      Routes.SettingsStoresAdd: (_) => const EditStore(),

      Routes.SettingsCategories: (_) => const CategoryList(),
      Routes.SettingsCategoriesAdd: (_) => const EditCategory(category: null),

      Routes.SettingsBackup: (_) => const Backup(),

      Routes.About: (_) => const About(),
    };
  }

  // Contains routes not covered by routes()
  static MaterialPageRoute<dynamic> onGenerateRoute(RouteSettings settings){
    switch(settings.name){
      case Routes.EditTransaction:
        final Transaction transaction = settings.arguments as Transaction;
        return MaterialPageRoute<dynamic>(builder: (_) => EditTransaction(transaction));
      case Routes.SettingsCategoriesEdit:
        final Category category = settings.arguments as Category;
        return MaterialPageRoute<dynamic>(builder: (_) => EditCategory(category: category));
      case Routes.SettingsStoresEdit:
        final Store store = settings.arguments as Store;
        return MaterialPageRoute<dynamic>(builder: (_) => EditStore(store: store));
      case Routes.SettingsStoresMerge:
        final Store store = settings.arguments as Store;
        return MaterialPageRoute<dynamic>(builder: (_) => MergeStore(toBeDeleted: store));
      case Routes.SettingsCategoriesMerge:
        final Category category = settings.arguments as Category;
        return MaterialPageRoute<dynamic>(builder: (_) => MergeCategory(toBeDeleted: category));
      default:
        throw Exception('This route is not implemented: ${settings.name}');
    }
  }

  static void navigateTo(BuildContext context, String route, { bool closeDrawer = false, Object argument }){
    if(closeDrawer)
      removeFromHistory(context);
    Navigator.pushNamed(context, route, arguments: argument);
  }

  static void removeFromHistory(BuildContext context){
    Navigator.pop(context);
  }
}