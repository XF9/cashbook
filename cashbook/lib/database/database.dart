library database;

import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:sqflite_porter/sqflite_porter.dart';

import '../helper/extensions.dart';

part 'models.dart';

typedef MigrationFunction = Future<void> Function(sqflite.Database db); 

class Database {
  factory Database() => _instance;
  Database._();  
  static final Database _instance = Database._();
  static sqflite.Database _database;

  var _updatedDatabase = false;

  Future<sqflite.Database> get instance async {
    if (_database != null) 
      return _database;

    _database = await _initDb();
    return _database;
  }

  Future<String> _getDbPath() async {
    return join(await sqflite.getDatabasesPath(), 'Transactions.db');
  }

  Future<sqflite.Database> _initDb() async {
    final sqflite.Database db = await sqflite.openDatabase(
        await _getDbPath(),
        version: getCurrentVersion(),
        onOpen: _onOpen,
        onCreate: _onCreate,
        onUpgrade: _onUpdate);

    if (!_updatedDatabase) 
      return db;

    _updatedDatabase = false;
    await db.close();
    return await _initDb();
  }

  void _onOpen(sqflite.Database db) {}

  Future _onCreate(sqflite.Database db, int version) async {
    await _onUpdate(db, 0, version);
  }

  Future _onUpdate(sqflite.Database db, int from, int to) async {
    _updatedDatabase = await migrate(db, from, to);
  }

  // Backup

  Future<List<String>> export() async {
    final result = [getCurrentVersion().toString()];
    final commands = await dbExportSql(_database);
    result.addAll(commands);
    return result;
  }

  Future import(List<String> lines) async {
    final version = int.parse(lines[0]);
    final commands = lines.skip(1).toList();
    final path = await _getDbPath();

    _database.close();
    _database = null;
    final oldDb = File.fromUri(Uri.file(path));
    oldDb.delete();

    final newDb = await sqflite.openDatabase(await _getDbPath(),
        version: version,
        onCreate: (sqflite.Database db, int version) async =>
            await dbImportSql(db, commands));
    newDb.close();
    instance;
  }

  // Migrations

  final List<MigrationFunction> _migrations = [
    _createInitialDatabase,
    _updateToVersion2,
    _updateToVersion3,
    _updateToVersion4,
    _updateToVersion5
  ];

  Future<bool> migrate(sqflite.Database db, int from, int to) async{
    final requiredScripts = _migrations.skip(from).take(to - from).toList();
    for(final script in requiredScripts){
      await script(db);
    }
    return requiredScripts.isNotEmpty;
  }

  int getCurrentVersion() => _migrations.length;

  static Future _createInitialDatabase(sqflite.Database db) async{
    await db.execute(
      'CREATE TABLE [Transaction] ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
        'value REAL,'
        'label TEXT,'
        'date TEXT'
      ')');
  }

  static Future _updateToVersion2(sqflite.Database db) async{
    final data = await db.rawQuery('SELECT id, date FROM [Transaction]');
    // sqllite dont support alter table
    // so we have to create a temporary table, migrate all data and drop the old one
    await db.transaction((txn) async {
      await db.execute(
        'CREATE TABLE [Transaction_tmp] ('
          'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
          'value REAL,'
          'label TEXT,'
          'date INTEGER'
        ')');
      await db.execute(
        'INSERT INTO [Transaction_tmp] (id, value, label)'
        'SELECT id, value, label FROM [Transaction]');
      for(final dataSet in data){
        await db.rawUpdate(
          'UPDATE [Transaction_tmp] SET date = ? WHERE id = ?',
          <dynamic>[
            DateFormat().parse(dataSet['date'].toString()).millisecondsSinceEpoch,
            dataSet['id']
          ]
        );
      }
      await db.execute('DROP TABLE [Transaction]');
      await db.execute('ALTER TABLE [Transaction_tmp] RENAME TO [Transaction]');
    });
  }

  static Future _updateToVersion3(sqflite.Database db) async{
    final data = await db.rawQuery('SELECT id, value, label, date FROM [Transaction]');
    final storeData = data
      .groupBy<String>((x) => x['label'].toString())
      .keys
      .toList();
    // Create store table
    await db.transaction((txn) async {
      await db.execute(
        'CREATE TABLE [Store] ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
        'name TEXT'
        ')');
      for(final store in storeData){
        await db.insert('Store', <String, dynamic>{'name': store});
      }

      await db.execute(
        'CREATE TABLE [Transaction_tmp] ('
          'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
          'value REAL,'
          'storeId INTEGER,'
          'date INTEGER'
        ')');
      await db.execute(
        'INSERT INTO [Transaction_tmp] (id, value, date)'
        'SELECT id, value, date FROM [Transaction]');
        
      for(final transaction in data){
        await db.rawUpdate(
          'UPDATE [Transaction_tmp] SET storeId = ? WHERE id = ?',
          <dynamic>[
            storeData.indexWhere((String store) => store == transaction['label'])+1,
            transaction['id']
          ]
        );
      }
      await db.execute('DROP TABLE [Transaction]');
      await db.execute('ALTER TABLE [Transaction_tmp] RENAME TO [Transaction]');
    });
  }

  static Future _updateToVersion4(sqflite.Database db) async{
    await db.transaction((txn) async {
      await db.execute(
        'CREATE TABLE [Transaction_tmp] ('
          'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
          'value REAL,'
          'storeId INTEGER,'
          'categoryId INTEGER,'
          'date INTEGER'
        ')');
      await db.execute(
        'INSERT INTO [Transaction_tmp] (id, value, storeId, date)'
        'SELECT id, value, storeId, date FROM [Transaction]');
      await db.execute('DROP TABLE [Transaction]');
      await db.execute('ALTER TABLE [Transaction_tmp] RENAME TO [Transaction]');

      await db.execute(
        'CREATE TABLE [Store_tmp] ('
          'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
          'name TEXT,'
          'categoryId INTEGER'
        ')');
      await db.execute('INSERT INTO [Store_tmp] (id, name)'
        'SELECT id, name FROM [Store]');
      await db.execute('DROP TABLE [Store]');
      await db.execute('ALTER TABLE [Store_tmp] RENAME TO [Store]');

      await db.execute(
        'CREATE TABLE [Category] ('
          'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
          'name TEXT,'
          'color INTEGER'
        ')');
    });
  }

  static Future _updateToVersion5(sqflite.Database db) async{
    await db.transaction((txn) async {
      await db.execute(
        'CREATE TABLE [Transaction_tmp] ('
          'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
          'value REAL,'
          'storeId INTEGER,'
          'categoryId INTEGER,'
          'date INTEGER,'
          'note TEXT'
        ')');
      await db.execute(
        'INSERT INTO [Transaction_tmp] (id, value, storeId, date, categoryId)'
        'SELECT id, value, storeId, date, categoryId FROM [Transaction]');
      await db.execute('DROP TABLE [Transaction]');
      await db.execute('ALTER TABLE [Transaction_tmp] RENAME TO [Transaction]');
    });
  }
}
