import 'dart:io';

import 'package:cashflow/helper/notifyChanged.dart';
import 'package:sqflite/sqflite.dart';

class FileBaseData{
  final DateTime confirmedTos;
  final DateTime nextBackupReminder;

  FileBaseData(this.confirmedTos, this.nextBackupReminder);
}

class FileBase extends NotifyChanged{
  Future<List<String>> _loadLines() async{
    final dir = await getDatabasesPath();
    final file = await File('$dir/data.txt').create(recursive: true);
    return await file.readAsLines();
  }

  Future _writeLines(List<String> lines) async{
    final dir = await getDatabasesPath();
    final file = await File('$dir/data.txt').create(recursive: true);
    return await file.writeAsString(lines.reduce((string, line) => '$string\n$line'));
  }

  Future<FileBaseData> load() async{
    final lines = await _loadLines();

    DateTime storedConfirmDate;
    if(lines.isNotEmpty)
      storedConfirmDate = lines[0] == ''
        ? null
        : DateTime.parse(lines[0]);

    DateTime nextBackupReminder;

    if(lines.length >= 2)
      nextBackupReminder = DateTime.parse(lines[1]);
    else{
      nextBackupReminder = DateTime.now().add(const Duration(days: 7));
      saveNextBackupReminder(nextBackupReminder);
    }

    return FileBaseData(storedConfirmDate, nextBackupReminder);
  }

  Future saveConfirmedTosDate(DateTime constentDate) async{
    final lines = await _loadLines();
    
    for(int index = lines.length; index < 1; index++)
      lines.add('');

    lines[0] = constentDate.toIso8601String();
    _writeLines(lines);
    notifyChanged();
  }

  Future saveNextBackupReminder(DateTime nextBackupReminder) async{
    final lines = await _loadLines();
    
    for(int index = lines.length; index < 2; index++)
      lines.add('');

    lines[1] = nextBackupReminder.toIso8601String();
    _writeLines(lines);
    notifyChanged();
  }
}