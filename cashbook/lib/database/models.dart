part of database;

class Store{
  Store({@required this.name, this.id, this.defaultCategory});

  int id;
  String name;
  Category defaultCategory;
}

class Transaction{
  Transaction(
    {
      @required this.date, 
      @required this.value, 
      this.id, 
      @required this.store, 
      @required this.note,
      this.category      
    }
  );
  
  final int id;
  final double value;
  final DateTime date;
  final Store store;
  final Category category;
  final String note;
}

class Category{
  Category({this.id, @required this.name, @required this.color});

  int id;
  String name;
  Color color;
}