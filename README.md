![](readme/header.png)

# CashFlow Meter

*Hey, your wallet is already empty and there is still some time till months end? You have no clue where your money went? Well this could be the app for your. Track all your expenses and income, categorize your transactions and know where your money went.*

## Google Play
It's finally available [on google play](https://play.google.com/store/apps/details?id=de.xf9.cashflow)!

# About

CashFlow Meter is a personal project of mine. It started out as a web service written in php and transformed into an android app written in Flutter. I did choose flutter mainly because I wanted to learn dart and it was quite the pleasant journey.

## Building

This app relies on flutter 1.22.6. Using another version may result in some goofy ui issues. It also uses [vscode-flutter-i18n-json](https://marketplace.visualstudio.com/items?itemName=esskar.vscode-flutter-i18n-json) to generate all language files from json files.